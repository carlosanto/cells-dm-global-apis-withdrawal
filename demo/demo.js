/* global mocks */
(function(w, d) {
  'use strict';

  var now = w.performance && w.performance.now
    ? w.performance.now.bind(w.performance)
    : Date.now;

  var _registration = [];
  d.addEventListener('register-step', function(e) {
    if (!_registration.find(function(item) {
      return item.index === e.detail.index;
    })) {
      _registration.push(e);
    }
  });

  d.addEventListener('WebComponentsReady', function(e) {

    var demo = d.querySelector('#demo');
    var dataManager = d.querySelector('#dataManager');
    var dataProvider = d.querySelector('#dpOwnTransfers');
    var demoScreen = d.querySelector('#demo-mobile .screen');
    var summary = d.querySelector('#summary');
    var SCROLL_TIME = 468;

    demo.set('page', 0);

    _registration.forEach(function(item) {
      dataManager.registerStep(item);
    });
    dataManager.set('accounts', mocks.accounts);

    dataProvider.generateRequest = function() {
      return new Promise(function(resolve, reject) {
        resolve({response: mocks.ownTransfer.post.response});
      });
    };

    //Animation
    d.addEventListener('cells-scroll-zone', function(evt) {
      var item = evt.detail.item;
      var config = evt.detail.config;

      ['#', '.', ''].some(function(prefix) {
        item = demoScreen.querySelector(prefix + item) || item;
        return item instanceof HTMLElement;
      });
      if (item instanceof HTMLElement) {
        new Promise(function(resolve, reject) {
          item.scrollIntoView(config);
          setTimeout(function() {
            resolve();
          }, (now() / SCROLL_TIME));
        }).then(function() {
          item.scrollComplete();
        });
      }
    }, false);

    d.addEventListener('cells-request-scroll-zone', function(evt) {
      dataManager.getLastActiveStep();
    }, false);
  });
}(window, document));
