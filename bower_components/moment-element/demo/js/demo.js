function myClick() {
  var myEl;
  var versionEl;

  myEl = document.querySelector('moment-element');
  document.querySelector('#version').innerHTML = myEl.m.version;

  document.querySelector('#case1').innerHTML =  myEl.m().format('MMMM Do YYYY, h:mm:ss a');
  document.querySelector('#case2').innerHTML =  myEl.m().format('dddd');
  document.querySelector('#case3').innerHTML =  myEl.m().format('MMM Do YY');
  document.querySelector('#case4').innerHTML =  myEl.m().format('YYYY [escaped] YYYY');
  document.querySelector('#case5').innerHTML =  myEl.m().format();

  document.querySelector('#case7').innerHTML = myEl.m('20111031', 'YYYYMMDD').fromNow();
  document.querySelector('#case8').innerHTML = myEl.m('20120620', 'YYYYMMDD').fromNow();
  document.querySelector('#case9').innerHTML = myEl.m().startOf('day').fromNow();
  document.querySelector('#case10').innerHTML = myEl.m().endOf('day').fromNow();
  document.querySelector('#case11').innerHTML = myEl.m().startOf('hour').fromNow();
  document.querySelector('#case11').innerHTML = myEl.m().subtract(10, 'days').calendar();
  document.querySelector('#case12').innerHTML = myEl.m().subtract(6, 'days').calendar();
  document.querySelector('#case13').innerHTML = myEl.m().subtract(3, 'days').calendar();
  document.querySelector('#case14').innerHTML = myEl.m().subtract(1, 'days').calendar();
  document.querySelector('#case15').innerHTML = myEl.m().calendar();
  document.querySelector('#case16').innerHTML = myEl.m().add(1, 'days').calendar();
  document.querySelector('#case17').innerHTML = myEl.m().add(3, 'days').calendar();
  document.querySelector('#case18').innerHTML = myEl.m().add(10, 'days').calendar();
  document.querySelector('#case19').innerHTML = myEl.m.locale();
  document.querySelector('#case20').innerHTML = myEl.m().format('LT');
  document.querySelector('#case21').innerHTML = myEl.m().format('LTS');
  document.querySelector('#case22').innerHTML = myEl.m().format('L');
  document.querySelector('#case23').innerHTML = myEl.m().format('l');
  document.querySelector('#case24').innerHTML = myEl.m().format('LL');
  document.querySelector('#case25').innerHTML = myEl.m().format('ll');
  document.querySelector('#case26').innerHTML = myEl.m().format('LLL');
  document.querySelector('#case27').innerHTML = myEl.m().format('lll');
  document.querySelector('#case28').innerHTML = myEl.m().format('LLLL');
  document.querySelector('#case29').innerHTML = myEl.m().format('llll');
}
