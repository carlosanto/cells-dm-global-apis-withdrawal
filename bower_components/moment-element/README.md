# moment-element

`<moment-element>` description. A moment JS library wrapper.

Example:

```html
<moment-element></moment-element>
```
```javascript
var this.$.moment = document.querySelector('moment-element');
console.log(this.$.moment.version);
console.log(this.$.moment().toNow());
```
 --moment-element  | empty mixin     | {}             |
