(function(d) {
  d.addEventListener('WebComponentsReady', function() {
    var demo = d.getElementById('demo');

    /* global mock1 */
    /* global mock2 */
    /* global mock3 */

    demo.mock1 = mock1;
    demo.mock2 = mock2;
    demo.mock3 = mock3;

    demo.changeLang = function(e) {
      document.documentElement.lang = e.target.value;
      I18nMsg.lang = e.target.value;
    };

    demo.logSelected = function(e) {
      demo.toastText = 'Selected operation: ' + e.detail.label;
      demo.$.toast.open();
    };

    demo.customAction = function(e) {
      e.preventDefault();
      demo.toastText = 'Custom action for "view more"';
      demo.$.toast.open();
    };

    demo.toastVisible = false;
  });
}(document));
