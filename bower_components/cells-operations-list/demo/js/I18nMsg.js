document.addEventListener('HTMLImportsLoaded', function() {
  I18nMsg.url = '../locales';
  I18nMsg.lang = document.documentElement.lang || 'en';
});
