var mock1 = [{
  label: 'Pagar',
  id: 'pay',
  icon: 'payment'
}, {
  label: 'Transferir',
  id: 'transfer',
  icon: 'play-for-work'
}, {
  label: 'Bloquear',
  id: 'lock',
  icon: 'receipt'
}, {
  label: 'Recargar',
  id: 'recharge',
  icon: 'record-voice-over'
}];

var mock2 = [{
  label: 'Pagar',
  id: 'pay',
  icon: 'query-builder'
}, {
  label: 'Transferir',
  id: 'transfer',
  icon: 'redeem'
}, {
  label: 'Bloquear tarjeta',
  id: 'lock',
  icon: 'perm-contact-calendar'
}, {
  label: 'Recargar móvil',
  id: 'recharge',
  icon: 'perm-camera-mic'
}, {
  label: 'Pagar',
  id: 'pay',
  icon: 'motorcycle'
}, {
  label: 'Transferir',
  id: 'transfer',
  icon: 'markunread'
}, {
  label: 'Bloquear',
  id: 'lock',
  icon: 'lock'
}, {
  label: 'Recargar',
  id: 'recharge',
  icon: 'toll'
}, {
  label: 'Pagar',
  id: 'pay',
  icon: 'perm-data-setting'
}, {
  label: 'Transferir',
  id: 'transfer',
  icon: 'more-vert'
}, {
  label: 'Bloquear',
  id: 'lock',
  icon: 'markunread'
}, {
  label: 'Recargar',
  id: 'recharge',
  icon: 'link'
}];

var mock3 = [{
  label: 'Editar alias',
  id: 'editAlias',
  icon: 'coronita:edit2'
}, {
  label: 'Bloquer tarjeta',
  id: 'lock',
  icon: 'coronita:padlock-locked'
}];
