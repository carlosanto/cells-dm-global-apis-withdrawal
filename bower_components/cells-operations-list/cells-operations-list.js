(function() {

  'use strict';

  Polymer({

    is: 'cells-operations-list',

    behaviors: [
      /* global CellsBehaviors */
      CellsBehaviors.i18nBehavior
    ],

    properties: {
      /**
       * List of operations.
       */
      operations: {
        type: Array,
        observer: '_operationsObserver'
      },

      /**
       * Number of columns in grid mode.
       */
      gridColumns: {
        type: Number,
        value: 4
      },

      /**
       * Icon size.
       */
      iconSize: {
        type: Number,
        value: 28
      },

      /**
       * List disposition: "grid" or "list".
       */
      layout: {
        type: String,
        value: 'grid',
        observer: '_layoutObserver',
        reflectToAttribute: true
      },

      /**
       * Maximum visible operations. Set to 0 to show all.
       */
      limit: {
        type: Number,
        value: 0,
        observer: '_limitObserver'
      },

      /**
       * Set to true to show a "view more" link. This item will be visible
       * if there are more available operations when the `limit` property is used.
       */
      viewMore: {
        type: Boolean,
        value: false
      },

      /**
       * Icon ID for the "view more" link.
       */
      viewMoreIcon: {
        type: String,
        value: 'more-horiz'
      },

      _showViewMore: {
        type: Boolean,
        value: false
      }
    },
    /**
     * add column width in function of the number of items
     * @param  {[Array]} array, number of items
     */
    _operationsObserver: function(array) {
      var numItems = array.length;
      var itemsPercent = 100 / numItems;

      if (this.gridColumns !== 4 && numItems >= 4) {
        itemsPercent = 100 / this.gridColumns;
      } else {
        if (numItems >= 4) {
          itemsPercent = 25;
        }
      }
      this.customStyle['--cells-operations-list-item-width'] = itemsPercent + '%';
      this.updateStyles();
    },

    _selectOperation: function(e) {
      e.preventDefault();
      this.fire('operation-click', e.model.item, {
        bubbles: false
      });
    },

    _layoutObserver: function(layout) {
      this.updateStyles();
    },

    _computeVisibility: function(index) {
      if (this.limit === 0) {
        return false;
      } else {
        var num = parseInt(index) + 1;

        // equal to the limit and not the last item
        if (num === this.limit && num !== this.operations.length) {
          return true;
        }

        return num > this.limit;
      }
    },

    _viewMore: function(e) {
      e.preventDefault();

      var firedEvent = this.fire('view-more', e, {cancelable: true});
      if (firedEvent.defaultPrevented) {
        return;
      }

      this.showAll();
    },

    /**
     * Show all operations.
     */
    showAll: function() {
      this.set('limit', 0);
    },

    _limitObserver: function(limit) {
      [].forEach.call(this.querySelectorAll('[data-index]'), function(item) {
        item.hidden = this._computeVisibility(item.dataset.index);
      }.bind(this));
    },

    _computeViewMore: function(viewMore, operations, limit) {
      return viewMore && (operations.length > limit && Boolean(limit)) ? true : false;
    }

    /**
     * Fired when the "view more" link is clicked.
     * Use `event.preventDefault()` to prevent showing all items (default action).
     * @event view-more
     * @param {Event} event
     */

    /**
     * Fired when an operation is clicked.
     * @event operation-click
     * @param {Object} detail Item model
     */

  });

}());
