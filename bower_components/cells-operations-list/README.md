# cells-operations-list

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

`<cells-operations-list>` displays a list in grid (default) or list mode with configurable length of available operations and an optional link to "show more" operations.
The "show more" link action shows the full list of operations by default, but can be customized by using `event.preventDefault()` on the `on-view-more` event.
Each operation has icon, label and ID.

Example:

```html
<cells-operations-list
  operations="[[operations]]"
  grid-columns="2"
  limit="4"
  view-more
  on-view-more="customAction">
</cells-operations-list>
```

## Data model

`operations` Array

```js
[{
 label: 'Transfer',
 id: 'transfer',
 icon: 'banking:A04'
}]
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property                                    | Description                                                 | Default |
|:---------------------------------------------------|:------------------------------------------------------------|:-------:|
| --cells-operations-list-item-width                 | variable for change items width                             | 100%    |
| --cells-operations-list-list__link-width           | variable for change label link items width                  | 70px    |
| --cells-operations-list-list                       | Mixin applied to the list element                           | {}      |
| --cells-operations-list-list__item                 | Mixin applied to each list item                             | {}      |
| --cells-operations-list-list__link                 | Mixin applied to each link                                  | {}      |
| --cells-operations-list-list__link-active          | Mixin applied to each link in active state                  | {}      |
| --cells-operations-list-item-label--grid           | Mixin applied to the item label in grid mode                | {}      |
| --cells-operations-list-item-label--list           | Mixin applied to the item label in list mode                | {}      |
| --cells-operations-list-list__item-not-first-child | Mixin applied to all the list items but not the first child | {}      |
| --cells-operations-list-list__item-not-last-child  | Mixin applied to all the list items but not the last child  | {}      |
