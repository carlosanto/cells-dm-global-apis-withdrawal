var products = [
  {
    'name': 'Cuenta personal C',
    'id': '2002',
    'imgSrc': '',
    'currencies': [
      {
        'currency': 'CLP',
        'isMajor': true
      }
    ],
    'description': {
      'value': '050400010100001604',
      'masked': true
    },
    'type': {
      'id': 'CREDIT_ACCOUNT',
      'name': 'Cuenta de crédito'
    },
    'group': 'account',
    'holds': [],
    'primaryAmount': {
      'amount': 40000000,
      'currency': 'CLP'
    },
    'actions': [
      {
        'action': 'action1',
        'icon': 'glomo:notification'
      },
      {
        'action': 'action2',
        'icon': 'glomo:bubble'
      },
      {
        'action': 'action3',
        'icon': 'glomo:nfc'
      }
    ]
  },
  {
    'name': 'Cuenta Pepito',
    'id': '2001',
    'imgSrc': '',
    'currencies': [
      {
        'currency': 'CLP',
        'isMajor': true
      }
    ],
    'description': {
      'value': '050400010100001604',
      'masked': true
    },
    'type': {
      'id': 'CREDIT_ACCOUNT',
      'name': 'Cuenta de crédito'
    },
    'group': 'account',
    'holds': [],
    'primaryAmount': {
      'amount': 65004000,
      'currency': 'CLP'
    },
    'actions': [
      {
        'action': 'action1',
        'icon': 'glomo:notification'
      },
      {
        'action': 'action2',
        'icon': 'glomo:bubble'
      },
      {
        'action': 'action3',
        'icon': 'glomo:nfc'
      }
    ]
  },
  {
    'name': 'Cuenta divisa',
    'id': '2004',
    'imgSrc': '',
    'currencies': [
      {
        'currency': 'USD',
        'isMajor': true
      },
      {
        'currency': 'CLP',
        'isMajor': false
      }
    ],
    'description': {
      'value': '050400010100001605',
      'masked': true
    },
    'type': {
      'id': 'CREDIT_CURRENCY_ACCOUNT',
      'name': 'Cuenta de crédito en divisa'
    },
    'group': 'account',
    'holds': [],
    'primaryAmount': {
      'amount': 2007.45,
      'currency': 'USD'
    },
    'actions': [
      {
        'action': 'action1',
        'icon': 'glomo:notification'
      },
      {
        'action': 'action2',
        'icon': 'glomo:bubble'
      },
      {
        'action': 'action3',
        'icon': 'glomo:nfc'
      }
    ]
  },
  {
    'name': 'Cuenta bono',
    'id': '2005',
    'imgSrc': '',
    'currencies': [
      {
        'currency': 'CLP',
        'isMajor': true
      }
    ],
    'description': {
      'value': '050400010100001606',
      'masked': true
    },
    'type': {
      'id': 'CREDIT_ACCOUNT',
      'name': 'Cuenta de crédito'
    },
    'group': 'account',
    'holds': [],
    'primaryAmount': {
      'amount': 12000000,
      'currency': 'CLP'
    },
    'actions': [
      {
        'action': 'action1',
        'icon': 'glomo:notification'
      },
      {
        'action': 'action2',
        'icon': 'glomo:bubble'
      },
      {
        'action': 'action3',
        'icon': 'glomo:nfc'
      }
    ]
  }
];

var productsEmpty = [];

var noProducts = null;

var templateBind = document.getElementById('tbind');

document.addEventListener('WebComponentsReady', function() {
  var el = document.querySelector('cells-product-selector');
  var tabs = document.querySelectorAll('paper-tab');
  var tab1 = tabs[0];
  var tab2 = tabs[1];
  var tab3 = tabs[2];

  tab1.addEventListener('click', function() {
    templateBind.set('products', products);
    templateBind.set('selected', 0);
  });

  tab2.addEventListener('click', function() {
    templateBind.set('products', productsEmpty);
    templateBind.set('selected', 1);
  });

  tab3.addEventListener('click', function() {
    templateBind.set('selected', 2);
  });

  // initializes the first tab
  tab1.click();
});
