/*global CellsBehaviors*/
(function() {

  'use strict';

  Polymer({

    is: 'cells-product-selector',

    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    properties: {

      /**
       * Account list to select from
       */
      products: {
        type: Array
      },

      /**
       * Header for the product list
       */
      listTitle: {
        type: String
      },

      /**
       * Index of the selected product
       * @type Number
       */
      selectedIndex: {
        type: Number
      },

      /**
       * Selected product
       * @type Number
       */
      selectedProduct: {
        type: Object
      },

      /**
       * Number of productsto show. Set to 0 to show all
       */
      maxProducts: {
        type: Number,
        value: 3
      },

      /**
       * Array containing the
       */
      _smallList: {
        type: Array
      },

      _hideSkeleton: {
        type: Boolean,
        value: false
      },

      _showMore: {
        type: Boolean,
        value: false
      },

      _hasZeroProducts: {
        type: Boolean,
        value: false
      },

      /**
       * Text for the no product title
       * @type {String}
       */
      message: String
    },

    observers: [
      '_productsObserver(products.splices)',
      '_maxProductsObserver(maxProducts, products)'
    ],

    _productsObserver: function() {
      if (this.products) {
        this._hasZeroProducts = this.products.length === 0;

        this._hideSkeleton = true;
        this._setDependentProps();
      } else {
        this._hideSkeleton = false;
      }
    },

    _setDependentProps: function() {
      this._showMore = this.maxProducts !== 0 && this.products.length > this.maxProducts;

      this._smallList = null;
      this.set('_smallList', this._showMore ? this.products.slice(0, this.maxProducts) : this.products);
    },

    _maxProductsObserver: function() {
      if (this.products) {
        this._setDependentProps();
      }
    },

    _onProductItemTap: function(evt) {
      /*istanbul ignore else*/
      if (this.products) {
        this.selectedIndex = parseInt(evt.currentTarget.dataset.index);
        this.selectedProduct = this.products[this.selectedIndex];
        this.fire('select-index', this.selectedIndex);
        this.fire('select-product', this.selectedProduct);
      }
    },

    _viewAll: function(e) {
      e.preventDefault();
      this.fire('view-all', this.products);
    }

    /**
     * @event select-index
     * @description fired when a product is selected. Contains the product's index
     */

    /**
     * @event select-product
     * @description fired when a product is selected. Contains the product
     */

    /**
     * @event view-all
     * @description fired when the view-all link is tapped. Contains the full product list
     */
  });

}());
