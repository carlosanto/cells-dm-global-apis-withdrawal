function setValues() {
  var cellsAmountInput = document.querySelector('#removableComponent');
  var selectCurrencyCode = document.querySelector('#selectCurrencyCode');
  var selectLocalCurrency = document.querySelector('#selectLocalCurrency');
  var selectLanguage = document.querySelector('#selectLanguage');
  var inputAmount = document.querySelector('#inputAmount');
  var dynamicEl = document.createElement('cells-molecule-amount-input');

  cellsAmountInput.remove();

  document.getElementById('container').appendChild(dynamicEl);

  // id only necesary for demo purposes
  dynamicEl.set('id', 'removableComponent');
  dynamicEl.set('label', 'With icon');
  dynamicEl.set('iconSize', 'icon-size-16');
  dynamicEl.set('iconCode', 'glomo:GM02');
  dynamicEl.set('currencyCode', selectCurrencyCode.value);
  dynamicEl.set('localCurrency', selectLocalCurrency.value);
  dynamicEl.set('language', selectLanguage.value);
  dynamicEl.set('setAmount', inputAmount.value);
}

window.addEventListener('WebComponentsReady', function() {
  'use strict';

  var button = document.querySelector('#button');
  var withoutIconComponent = document.querySelector('#withoutIconComponent');
  var disabledComponent = document.querySelector('#disabledComponent');
  var disabledComponent2 = document.querySelector('#disabledComponent2');

  button.addEventListener('click', setValues);

  withoutIconComponent.set('setAmount', 1154000.35);
  disabledComponent.set('setAmount', 1154000.35);
});