/* global CellsBehaviors, Cleave */
Polymer({
  is: 'cells-molecule-amount-input',

  behaviors: [
    CellsBehaviors.AmountBehavior
  ],

  properties: {
    /**
     * Amount set in the input from the outside.
     * @type {Number}
     */
    setAmount: {
      type: Number,
      observer: '_setAmountObserver'
    },

    /**
     * The formatted amount for the input.
     * Is the value visible in the input.
     * @type {String}
     */
    _formattedAmount: {
      type: String,
      observer: '_formattedAmountObserver'
    },

    /**
     * Output amount.
     * @type {Number}
     */
    amount: {
      type: Number,
      notify: true
    },

    /**
     * Label text of the input
     * @type {String}
     */
    label: String,

    /**
     * Name that maps to HTML native's name attribute (useful for forms).
     * Mandadatory if there are more than one input and needs to be unique to avoid conflicts with cleave.js
     * @type {String}
     */
    name: {
      type: String,
      value: 'amount'
    },

    /**
     * If true, the element currently has focus.
     * Reflect to attribute.
     * @type {Boolean}
     */
    _focused: {
      type: Boolean,
      value: false
    },

    /**
     * Control variable that indicates if input is empty
     * @type {Boolean}
     */
    _isFilled: {
      type: Boolean,
      value: false
    },

    /**
     * If true, the element is disabled.
     * Reflect to attribute.
     * @type {Boolean}
     */
    disabled: {
      type: Boolean,
      value: false,
      reflectToAttribute: true
    },

    /**
     * If true, the element is required.
     * Reflect to attribute.
     * @type {Boolean}
     */
    required: {
      type: Boolean,
      value: false,
      reflectToAttribute: true
    },

    /**
     * The icon code of the icon-set. View documentation for the icon component
     * @type {String}
     */
    iconCode: String,

    /**
     * The icon size of the icon. View documentation for the icon component
     * @type {String}
     */
    iconSize: String,

    /**
     * ISO 4217 code for the currency
     * @type {String}
     */
    currencyCode: String,

    /**
     * ISO 4217 code for the local currency
     * @type {String}
     */
    localCurrency: String,

    /**
     * Language for the currency
     * @type {String}
     */
    language: String,

    /**
     * Behavior function than returns the formmated currency
     * @type {String}
     */
    _currency: {
      type: String,
      computed: '_getCurrencyAsSymbol(localCurrency, currencyCode)'
    },

    /**
     * Controls the currency visibility
     * @type {Boolean}
     */
    _showCurrencies: {
      type: Boolean,
      value: false
    },

    /**
     * Stores cleave.js than formats the input
     * @type {Object}
     */
    _cleave: Object
  },

  listeners: {
    'input.focus': '_onInputFocus',
    'input.blur': '_onInputBlur',
    'icon.click': '_onIconClick'
  },

  /**
   */
  detached: function() {
    if (this._cleave) {
      this._cleave.destroy();
    }
  },

  /**
   * Observer for set amount. Activated when amount is set from the outside.
   * Sets _formattedAmount
   * @param {number} amount The amount than is set
   */
  _setAmountObserver: function(amount) {
    if (this._isAmountValid(amount)) {
      amount = this._checksForForeingMarkDecimal(amount);

      this.set('_formattedAmount', amount);
    } else {
      this.set('setAmount', undefined);
      this.set('_formattedAmount', undefined);
      this.set('amount', undefined);
    }
  },

  /**
   * Validates if the amount is valid.
   * Converts it to string.
   * @param {number} number Number to validate
   * @return {boolean} If is amount valid.
   */
  _isAmountValid: function(amount) {
    var numberValidation = /^-{0,1}\d*\.{0,1}\d+$/;

    if (amount) {
      amount = amount.toString();
    }

    return numberValidation.test(amount);
  },

  /**
   * Validates if the amount is valid.
   * Converts it to string.
   * @param {number} number Number to validate
   * @return {string} amount The amount.
   */
  _checksForForeingMarkDecimal: function(amount) {
    var language = this.language;

    if (language) {
      var decimalMark = this._getSeparator(language);

      amount = amount.toString();

      if (decimalMark !== '.') {
        amount = amount.replace('.', decimalMark);
      }

      return amount;
    }
  },

  /**
   * Observer for formatted amount. Activated when input is filled or amount changes.
   * Formats the input.
   * Stores amount, the amount without formatting.
   * Check status
   * @param {string} number Amount
   */
  _formattedAmountObserver: function(amount) {
    this._checksStatus(amount);

    this._inicializeFormatIntput();

    this._setOutputAmount();
  },

  /**
   * Checks amount and set status.
   * @param {string} number Amount
   */
  _checksStatus: function(amount) {
    this._toggleHasContentClass(true);
    if (amount) {
      this.set('_isFilled', true);
      this.set('_showCurrencies', true);
    } else if (amount === '' || amount === undefined) {
      this.set('_isFilled', false);
      this.set('_showCurrencies', this._focused);
      this._toggleHasContentClass(this._focused);
    }
  },

  /**
   * Stores amount, the raw amount without formatting.
   * Limits the amount to 2 decimals.
   * Fires 'amount-input-changed' event.
   * @event amount-input-changed
   * @param {number} amount The amount than is set
   */
  _setOutputAmount: function() {
    var inputAmount = this._cleave.getRawValue();
    var hasDecimals;
    var amountDecimal;

    if (!isNaN(inputAmount) && inputAmount !== '') {
      amountDecimal = inputAmount.indexOf('.');
      hasDecimals = amountDecimal !== -1;

      if (hasDecimals) {
        inputAmount = inputAmount.slice(0, (amountDecimal) + 3);
      }

      inputAmount = parseFloat(inputAmount);
    } else {
      inputAmount = undefined;
    }

    this.set('amount', inputAmount);
    this.fire('amount-input-changed', inputAmount);
  },

  /**
   * Inicialize cleave.js to format the input whin the correct configuration
   */
  _inicializeFormatIntput: function() {
    var language = this.language;

    var options = {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand',
      numeralDecimalMark: this._getSeparator(language),
      delimiter: this._getGroupChars(language)
    };

    this._cleave = new Cleave(this.$.input, options);
  },

  /**
   * Method to focus to the input.
   */
  focus: function() {
    this.$.input.focus();
  },

  /**
   * Method to blur at the input.
   */
  blur: function() {
    this.$.input.blur();
  },

  /**
   * Handles a 'click' event on the icon.
   * Reset's the input by cleaning its content.
   * @param {event} e Event
   */
  _onIconClick: function() {
    this.set('_formattedAmount', '');

    this.focus();
  },

  /**
   * Handles a 'input focus' event.
   * @param {event} e Event focus
   */
  _onInputFocus: function() {
    if (!this.disabled) {
      this.set('_focused', true);
      this._checksStatus(this.amount);
    }
  },

  /**
   * Handles a 'input blur' event.
   * @param {event} e Event blur
   */
  _onInputBlur: function(e) {
    e.preventDefault();
    this.set('_focused', false);
    this._checksStatus(this.amount);
  },

  /**
   * Toggle the has-content css class in the input
   * @param {Boolean} hasContent Toggle class
   */
  _toggleHasContentClass: function(hasContent) {
    this.toggleClass('has-content', hasContent);
    this.toggleClass('has-content', hasContent, this.$.input);
  }
});
