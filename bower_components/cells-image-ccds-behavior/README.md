# cells-image-ccds-behavior

Adds width and height to request's depending on the device pixel ratio and the image containers dimensions.

## Usage
In your Polymer component, just add it to your 'behaviors' array:

```javascript
behaviors: [
  Polymer.CCDSImagesBehavior
]
```
