var products = {
  items: [
    {
      'accountId': '2002',
      'number': '050400010100001604',
      'numberType': {
        'id': 'LIC',
        'name': 'Local Identification Code'
      },
      'formats': [
        {
          'numberType': {
            'id': 'SIMPLE_ACCOUNT'
          },
          'number': '666 666 666'
        }
      ],
      'accountType': {
        'id': 'CREDIT_ACCOUNT',
        'name': 'Cuenta de crédito'
      },
      'title': {
        'id': 'CREDITO_CL',
        'name': 'Línea de crédito'
      },
      'bank': {
        'name': 'BBVA Chile'
      },
      'alias': 'Cuenta personal C',
      'address': {
        'addressName': 'Av. Pte.Roque Saenz Pena 991',
        'city': 'Santiago',
        'state': 'Región Metropolitana',
        'country': {
          'id': 'CL',
          'name': 'Chile'
        },
        'zipCode': '3580000',
        'alias': 'Dirección actual'
      },
      'openingDate': '2016-05-12T11:02:32.096Z',
      'currencies': [
        {
          'currency': 'CLP',
          'isMajor': true
        }
      ],
      'grantedCredits': [
        {
          'amount': 80000000,
          'currency': 'CLP'
        }
      ],
      'availableBalance': {
        'currentBalances': [
          {
            'amount': 40000000,
            'currency': 'CLP'
          }
        ],
        'postedBalances': [
          {
            'amount': 40000000,
            'currency': 'CLP'
          }
        ],
        'pendingBalances': [
          {
            'amount': 0,
            'currency': 'CLP'
          }
        ]
      },
      'disposedBalance': {
        'currentBalances': [
          {
            'amount': 40000000,
            'currency': 'CLP'
          }
        ],
        'postedBalances': [
          {
            'amount': 40000000,
            'currency': 'CLP'
          }
        ],
        'pendingBalances': [
          {
            'amount': 0,
            'currency': 'CLP'
          }
        ]
      },
      'paymentMethod': {
        'paymentType': {
          'id': 'FREE_AMOUNT_PAYMENT',
          'name': 'Free amount payment'
        },
        'period': {
          'id': 'MONTHLY',
          'name': 'mensual'
        },
        'endDate': '2016-09-12T11:02:32.096Z',
        'paymentAmounts': [
          {
            'id': 'MINIMUM_AMOUNT',
            'name': 'Pago mínimo',
            'values': [
              {
                'amount': 20000,
                'currency': 'CLP'
              }
            ]
          },
          {
            'id': 'LAST_PERIOD_AMOUNT',
            'name': 'Deuda de este mes',
            'values': [
              {
                'amount': 50000,
                'currency': 'CLP'
              }
            ]
          },
          {
            'id': 'TOTAL_DEBT_AMOUNT',
            'name': 'Saldo al corte',
            'values': [
              {
                'amount': 53000,
                'currency': 'CLP'
              }
            ]
          }
        ]
      },
      'status': {
        'id': 'ACTIVATED',
        'name': 'Cuenta operativa'
      },
      'relatedContracts': [
        {
          'relatedContractId': '8001',
          'contractId': '2001',
          'number': '050400010100001603',
          'numberType': {
            'id': 'LIC',
            'name': 'Local Identification Code'
          },
          'product': {
            'id': 'ACCOUNTS',
            'name': 'Cuentas'
          },
          'relationType': {
            'id': 'LINKED_WITH',
            'name': 'Linked with'
          }
        }
      ],
      'limits': [],
      'holds': [],
      'customizedFormats': []
    },
    {
      'accountId': '2004',
      'number': '050400010100001605',
      'numberType': {
        'id': 'LIC',
        'name': 'Local Identification Code'
      },
      'accountMobile': true,
      'accountType': {
        'id': 'CREDIT_CURRENCY_ACCOUNT',
        'name': 'Cuenta de crédito en divisa'
      },
      'title': {
        'id': 'CREDIT_DIVISA_CL',
        'name': 'Cuenta de crédito en divisa'
      },
      'bank': {
        'name': 'BBVA Chile'
      },
      'alias': 'Cuenta divisa',
      'address': {
        'addressName': 'Av. Pte.Roque Saenz Pena 991',
        'city': 'Santiago',
        'state': 'Región Metropolitana',
        'country': {
          'id': 'CL',
          'name': 'Chile'
        },
        'zipCode': '3580000',
        'alias': 'Dirección actual'
      },
      'openingDate': '2016-02-18T17:45:12.096Z',
      'currencies': [
        {
          'currency': 'USD',
          'isMajor': true
        },
        {
          'currency': 'CLP',
          'isMajor': false
        }
      ],
      'grantedCredits': [
        {
          'amount': 25255.55,
          'currency': 'USD'
        },
        {
          'amount': 16984232,
          'currency': 'CLP'
        }
      ],
      'availableBalance': {
        'currentBalances': [
          {
            'amount': 2007.45,
            'currency': 'USD'
          },
          {
            'amount': 1350000,
            'currency': 'CLP'
          }
        ],
        'postedBalances': [
          {
            'amount': 2007.45,
            'currency': 'USD'
          },
          {
            'amount': 1350000,
            'currency': 'CLP'
          }
        ],
        'pendingBalances': [
          {
            'amount': 0,
            'currency': 'USD'
          },
          {
            'amount': 0,
            'currency': 'CLP'
          }
        ]
      },
      'disposedBalance': {
        'currentBalances': [
          {
            'amount': 23248.1,
            'currency': 'USD'
          },
          {
            'amount': 15634232,
            'currency': 'CLP'
          }
        ],
        'postedBalances': [
          {
            'amount': 23248.1,
            'currency': 'USD'
          },
          {
            'amount': 15634232,
            'currency': 'CLP'
          }
        ],
        'pendingBalances': [
          {
            'amount': 0,
            'currency': 'USD'
          },
          {
            'amount': 0,
            'currency': 'CLP'
          }
        ]
      },
      'paymentMethod': {
        'paymentType': {
          'id': 'FREE_AMOUNT_PAYMENT',
          'name': 'Free amount payment'
        },
        'period': {
          'id': 'MONTHLY',
          'name': 'mensual'
        },
        'endDate': '2016-10-12T11:02:32.096Z',
        'paymentAmounts': [
          {
            'id': 'MINIMUM_AMOUNT',
            'name': 'Pago mínimo',
            'values': [
              {
                'amount': 200,
                'currency': 'USD'
              }
            ]
          },
          {
            'id': 'LAST_PERIOD_AMOUNT',
            'name': 'Deuda de este mes',
            'values': [
              {
                'amount': 500,
                'currency': 'USD'
              }
            ]
          },
          {
            'id': 'TOTAL_DEBT_AMOUNT',
            'name': 'Saldo al corte',
            'values': [
              {
                'amount': 530,
                'currency': 'USD'
              }
            ]
          }
        ]
      },
      'status': {
        'id': 'ACTIVATED',
        'name': 'Cuenta operativa'
      },
      'relatedContracts': [],
      'limits': [],
      'holds': [],
      'customizedFormats': []
    },
    {
      'accountId': '2005',
      'number': '050400010100001606',
      'numberType': {
        'id': 'LIC',
        'name': 'Local Identification Code'
      },
      'accountType': {
        'id': 'CREDIT_ACCOUNT',
        'name': 'Cuenta de crédito'
      },
      'title': {
        'id': 'BONO_FACIL_CL',
        'name': 'Cuenta bono fácil'
      },
      'bank': {
        'name': 'BBVA Chile'
      },
      'alias': 'Cuenta bono',
      'address': {
        'addressName': 'Av. Pte.Roque Saenz Pena 991',
        'city': 'Santiago',
        'state': 'Región Metropolitana',
        'country': {
          'id': 'CL',
          'name': 'Chile'
        },
        'zipCode': '3580000',
        'alias': 'Dirección actual'
      },
      'openingDate': '2016-02-18T17:45:12.096Z',
      'currencies': [
        {
          'currency': 'CLP',
          'isMajor': true
        }
      ],
      'grantedCredits': [
        {
          'amount': 22000000,
          'currency': 'CLP'
        }
      ],
      'availableBalance': {
        'currentBalances': [
          {
            'amount': 12000000,
            'currency': 'CLP'
          }
        ],
        'postedBalances': [
          {
            'amount': 12000000,
            'currency': 'CLP'
          }
        ],
        'pendingBalances': [
          {
            'amount': 0,
            'currency': 'CLP'
          }
        ]
      },
      'disposedBalance': {
        'currentBalances': [
          {
            'amount': 10000000,
            'currency': 'CLP'
          }
        ],
        'postedBalances': [
          {
            'amount': 10000000,
            'currency': 'CLP'
          }
        ],
        'pendingBalances': [
          {
            'amount': 0,
            'currency': 'CLP'
          }
        ]
      },
      'paymentMethod': {
        'paymentType': {
          'id': 'FREE_AMOUNT_PAYMENT',
          'name': 'Free amount payment'
        },
        'period': {
          'id': 'MONTHLY',
          'name': 'mensual'
        },
        'endDate': '2016-09-12T11:02:32.096Z',
        'paymentAmounts': [
          {
            'id': 'MINIMUM_AMOUNT',
            'name': 'Pago mínimo',
            'values': [
              {
                'amount': 22000,
                'currency': 'CLP'
              }
            ]
          },
          {
            'id': 'LAST_PERIOD_AMOUNT',
            'name': 'Deuda de este mes',
            'values': [
              {
                'amount': 54000,
                'currency': 'CLP'
              }
            ]
          },
          {
            'id': 'TOTAL_DEBT_AMOUNT',
            'name': 'Saldo al corte',
            'values': [
              {
                'amount': 57000,
                'currency': 'CLP'
              }
            ]
          }
        ]
      },
      'status': {
        'id': 'ACTIVATED',
        'name': 'Cuenta operativa'
      },
      'relatedContracts': [],
      'limits': [
        {
          'limitId': 'MAXIMUM_CREDIT',
          'name': 'Cupo máximo',
          'amountLimits': [
            {
              'amount': 6790000,
              'currency': 'CLP'
            }
          ]
        }
      ],
      'holds': [],
      'customizedFormats': []
    },
    {
      'accountId': '2001',
      'number': '050400010100001603',
      'numberType': {
        'id': 'LIC',
        'name': 'Local Identification Code'
      },
      'accountType': {
        'id': 'COMMON_ACCOUNT',
        'name': 'Cuenta corriente'
      },
      'title': {
        'id': 'CORRIENTE_LCREDITO_CL',
        'name': 'Cuenta corriente con línea de crédito'
      },
      'bank': {
        'name': 'BBVA Chile'
      },
      'address': {
        'addressName': 'Av. Pte.Roque Saenz Pena 991',
        'city': 'Santiago',
        'state': 'Región Metropolitana',
        'country': {
          'id': 'CL',
          'name': 'Chile'
        },
        'zipCode': '3580000',
        'alias': 'Dirección actual'
      },
      'openingDate': '2016-01-08T11:02:32.096Z',
      'currencies': [
        {
          'currency': 'CLP',
          'isMajor': true
        }
      ],
      'availableBalance': {
        'currentBalances': [
          {
            'amount': 1700000,
            'currency': 'CLP'
          }
        ],
        'postedBalances': [
          {
            'amount': 1725000,
            'currency': 'CLP'
          }
        ],
        'pendingBalances': [
          {
            'amount': 25000,
            'currency': 'CLP'
          }
        ]
      },
      'status': {
        'id': 'ACTIVATED',
        'name': 'Cuenta operativa'
      },
      'relatedContracts': [
        {
          'relatedContractId': '8001',
          'contractId': '2002',
          'number': '050400010100001604',
          'numberType': {
            'id': 'LIC',
            'name': 'Local Identification Code'
          },
          'product': {
            'id': 'ACCOUNTS',
            'name': 'Cuentas'
          },
          'relationType': {
            'id': 'LINKED_WITH',
            'name': 'Linked with'
          }
        },
        {
          'relatedContractId': '8002',
          'contractId': '2101',
          'number': '4318573837653283',
          'numberType': {
            'id': 'PAN',
            'name': 'Permanent Account Number'
          },
          'product': {
            'id': 'CARDS',
            'name': 'Tarjetas'
          },
          'relationType': {
            'id': 'LINKED_WITH',
            'name': 'Linked with'
          }
        },
        {
          'relatedContractId': '8003',
          'contractId': '2102',
          'number': '4172752399835476',
          'numberType': {
            'id': 'PAN',
            'name': 'Permanent Account Number'
          },
          'product': {
            'id': 'CARDS',
            'name': 'Tarjetas'
          },
          'relationType': {
            'id': 'LINKED_WITH',
            'name': 'Linked with'
          }
        }
      ],
      'limits': [],
      'holds': [],
      'customizedFormats': []
    },
    {
      'accountId': '2003',
      'number': '050400010100001605',
      'numberType': {
        'id': 'LIC',
        'name': 'Local Identification Code'
      },
      'accountType': {
        'id': 'CURRENCY_ACCOUNT',
        'name': 'Cuenta corriente'
      },
      'title': {
        'id': 'DIVISA_CL',
        'name': 'Cuenta en divisa'
      },
      'bank': {
        'name': 'BBVA Chile'
      },
      'alias': 'Cuenta en dólares',
      'address': {
        'addressName': 'Av. Pte.Roque Saenz Pena 991',
        'city': 'Santiago',
        'state': 'Región Metropolitana',
        'country': {
          'id': 'CL',
          'name': 'Chile'
        },
        'zipCode': '3580000',
        'alias': 'Dirección actual'
      },
      'openingDate': '2016-01-08T11:02:32.096Z',
      'currencies': [
        {
          'currency': 'USD',
          'isMajor': true
        },
        {
          'currency': 'CLP',
          'isMajor': false
        }
      ],
      'availableBalance': {
        'currentBalances': [
          {
            'amount': 2527.9,
            'currency': 'USD'
          },
          {
            'amount': 1700000,
            'currency': 'CLP'
          }
        ],
        'postedBalances': [
          {
            'amount': 2565.08,
            'currency': 'USD'
          },
          {
            'amount': 1725000,
            'currency': 'CLP'
          }
        ],
        'pendingBalances': [
          {
            'amount': 37.9,
            'currency': 'USD'
          },
          {
            'amount': 25000,
            'currency': 'CLP'
          }
        ]
      },
      'status': {
        'id': 'ACTIVATED',
        'name': 'Cuenta operativa'
      },
      'relatedContracts': [],
      'limits': [],
      'holds': [],
      'customizedFormats': [
        {
          'customizedFormatId': 'SIMPLE_ACCOUNT',
          'value': '5166666'
        }
      ]
    }
  ]
};
