# cells-step-product-selector

Your component description.

Example:
```html
<cells-step-product-selector></cells-step-product-selector>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-step-product-selector-scope      | scope description | default value  |
| --cells-step-product-selector  | empty mixin     | {}             |
