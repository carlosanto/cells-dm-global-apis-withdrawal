/* global CellsBehaviors */

(function() {

  'use strict';

  Polymer({

    is: 'cells-step-product-selector',

    behaviors: [
      CellsBehaviors.i18nBehavior,
      CellsBehaviors.StepBehavior,
      Polymer.ProductsBehavior
    ],

    properties: {

      /**
       * @type [Array]
       * @description products to select from
       */
      products: {
        type: Array,
        observer: '_productsChanged'
      },

      _products: Array,

      /**
       * @type [Object]
       * @description selected product
       */
      selected: {
        notify: true,
        type: String
      },

      _selected: Object,

      /**
       * @type [String]
       * @description title for the product list
       */
      listTitle: {
        type: String,
        value: ''
      },

      /**
       * @type {String}
       * @desc selected product's index
       */
      productIndex: {
        type: String
      },

      /**
       * @type {String}
       * @desc default selected product's id. Not erased on reset
       */
      defaultProductId: {
        type: String
      },

      /**
       * @type {String}
       * @desc default selected product's index. Not erased on reset
       */
      defaultProductIndex: {
        type: String
      },

      /**
       * @type Boolean
       * helper property to hide/show the change button
       */
      _changeHidden: {
        type: Boolean,
        value: false
      }
    },

    listeners: {
      'select-product': '_onProductSelected',
      'stepContainer.change-pressed': '_onChangeProduct',
      'selector.view-all': '_viewMorePressed'
    },

    observers: [
      '_indexObserver(productIndex, _products, active)',
      '_indexObserver(defaultProductIndex, _products, active)',
      '_idOberver(defaultProductId, _products, active)',
      '_onCollapsedChanged(collapsed)'
    ],

    attached: function() {
      this.getMsg('cells-step-product-selector-my-accounts').then(function(translation) {
        if (translation !== null) {
          this.set('listTitle', translation);
        } else {
          this.set('listTitle', 'My BBVA accounts');
        }
      }.bind(this));
    },

    /**
     * Listen if view more button is pressed and fires the action if step is active.
     * @param {Event} event View more pressed
     * @event view-all
     */
    _viewMorePressed: function(evt) {
      evt.stopPropagation();

      if (this.active) {
        this.fire('view-all', evt.detail);
      }
    },

    _onCollapsedChanged: function(isCollapsed) {
      if (isCollapsed && this.active && !this.selected && this.isValid()) {
        this.set('selected', this.get('_selected.id'));
      }
    },

    /**
     * @override
     * @desc resets the component's state
     */
    reset: function() {
      var index = this.defaultProductIndex;
      var cellsProductSelector = this.$.selector;

      this.set('selected', null);
      this.set('_selected', null);
      this.set('collapsed', false);
      this.set('productIndex', null);
      this.set('products', null);
      this.set('_products', null);
      cellsProductSelector.set('products', null);
      cellsProductSelector.set('_smallList', null);

      if (index !== undefined) {
        this.set('defaultProductIndex', index);
      }
    },

    /**
     * @override
     * @desc checks whether the user has set a valid input
     */
    isValid: function() {
      return !!this._selected;
    },

    _idOberver: function(id, _products) {
      var index;
      var i = 0;

      if (_products) {
        while (index === undefined && i < _products.length) {
          if (_products[i].id === id) {
            index = i;
          }
          i++;
        }

        if (index !== undefined) {
          this.set('defaultProductIndex', index);
        }
      }
    },

    _indexObserver: function(index, _products) {
      if (index !== null && Array.isArray(_products) && _products.length > 0) {
        var evt = {detail: _products[index]};
        this._onProductSelected(evt);

        if (this.defaultProductIndex !== null && this.defaultProductIndex !== undefined) {
          this.set('fixed', true);
        }
      }
    },

    _onProductSelected: function(evt) {
      if (this.active) {
        this.set('selected', evt.detail.id);
        this.set('_selected', evt.detail);
        this.set('collapsed', true);
      }
    },

    _onChangeProduct: function() {
      this.set('selected', null);
    },

    _productsChanged: function(products) {
      this.set('selected', null);
      this.set('_selected', null);

      if (products) {
        products = products.items || products;

        this._changeHidden = this.fixed || products.length === 1;

        this.set('collapsed', false);

        this._products = products instanceof Array ? this._formatProducts(products, this._parseAccounts.bind(this)) : null;

        if (this._products.length === 1) {
          this._indexObserver(0, this._products);
        }
      }
    },

    _parseAccounts: function(account, originalAccount, currency) {
      var amount = this._getBalanceByCurrency(originalAccount.availableBalance.currentBalances, currency);

      account.imgSrc = '';

      account.primaryAmount = {
        amount: amount.amount,
        currency: amount.currency
      };

      var simpleNumber = this._getSimpleNumber(originalAccount);
      if (simpleNumber) {
        account.simpleNumber = simpleNumber;
        account.description = null;
      }

      if (originalAccount.accountMobile) {
        account.icon = 'glomo:mobile';
      }

      if (!account.name) {
        account.name = this._getProductName(originalAccount, 'cells-global-dashboard-account-description', 'Cuenta BBVA');
      }
    }

    /**
     * @event select-changed
     * @description fired when a product is selected. Contains the product's ID
     */

  });

}());
