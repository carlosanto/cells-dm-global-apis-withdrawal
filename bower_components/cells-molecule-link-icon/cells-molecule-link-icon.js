Polymer({

  is: 'cells-molecule-link-icon',
  extends: 'a',

  properties: {

    /**
     * Name of the icon to show
     */
    icon: {
      type: String,
      value: 'A01'
    }
  }

});
