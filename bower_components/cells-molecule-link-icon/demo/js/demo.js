var prevMock = null;

function setMock(key) {

  key = key || 'mock1';
  var contentBox = document.querySelector('#' + key);

  if (prevMock !== null) {
    prevMock.hidden = true;
  }

  prevMock = contentBox;

  contentBox.hidden = false;

}

// Switch theme
// var tabs, defaultTab, customTab, ironPages;
// defaultTab = document.querySelector('.defaultTab');
var tabsSwitcher = document.querySelector('paper-tabs');
tabsSwitcher.addEventListener('click', function(event) {
  setMock(event.target.parentElement.dataset.mockkey);
});

setMock();
