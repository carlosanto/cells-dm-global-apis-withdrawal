# cells-step-behavior

===================

`CellsBehaviors.stepBehavior` sets a common interface for operative step elements.

#### Import

1) Import the behavior in your component:

```html
<link rel="import" href="../cells-step-behavior/cells-step-behavior.html">
```

2) Add CellsBehaviors.i18nBehavior to the behaviors list in the JS file or script of your component:

```js
behaviors: [CellsBehaviors.stepBehavior]
```


# cells-step-manager-behavior

===================

`CellsBehaviors.StepManagerBehavior` sets a common interface for operatives managers.

#### Import

1) Import the behavior in your component:

```html
<link rel="import" href="../cells-step-behavior/cells-step-manager-behavior.html">
```

2) Add CellsBehaviors.StepManagerBehavior to the behaviors list in the JS file or script of your component:

```javascript
behaviors: [CellsBehaviors.StepManagerBehavior]
```