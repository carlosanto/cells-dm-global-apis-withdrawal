/* global CellsBehaviors */

(function() {

  'use strict';

  Polymer({

    is: 'cells-step-enter-amount',

    behaviors: [
      CellsBehaviors.StepBehavior
    ],

    properties: {
      /**
       * Amount set in the input from the outside.
       * @type {Number}
       */
      setAmount: {
        type: Number
      },

      /**
       * Output amount entered.
       * Setted when press the button and input is valid.
       * @type {Number}
       */
      amount: {
        type: Number,
        notify: true
      },

      _amount: {
        type: Number
      },

      /**
       * Total amount available.
       * @type {Number}
       */
      totalBalance: Number,

      /**
       * ISO 4217 code for the currency
       * @type {String}
       */
      currency: String,

      /**
       * ISO 4217 code for the local currency
       * @type {String}
       */
      localCurrency: {
        type: String
      },

      /**
       * The icon code of the icon-set. View documentation for the icon component
       * @type {String}
       */
      iconCode: {
        type: String,
        value: 'glomo:GM02'
      },

      /**
       * The icon size of the icon. View documentation for the icon component
       * @type {String}
       */
      iconSize: {
        type: String,
        value: 'icon-size-16'
      },

      /**
       * Language for the currency
       * @type {String}
       */
      language: {
        type: String
      },

      /**
       * Name required in amount component
       * Mandadatory and needs to be unique to avoid conflicts with cleave.js
       * @type {String}
       */
      name: {
        type: String,
        value: 'stepEnterAmount'
      },

      /**
       * Is valid property
       * @type {Boolean}
       */
      _isValid: {
        type: Boolean,
        value: false
      },

      /**
       * Max limit for the operation.
       * Optional
       * @type {Number}
       */
      maxLimit: {
        type: Number
      },

      /**
       * Min limit for the operation.
       * Optional
       * @type {Number}
       */
      minLimit: {
        type: Number
      }
    },

    listeners: {
      'change-pressed': '_onChangePressed',
      'send-amount': '_onSendAmount',
      'amount-input-changed': '_setAmount'
    },

    observers: [
      '_activeChanged(active)',
      '_onCollapsedChanged(collapsed)'
    ],

    /**
     * Sets the input amount in the component.
     * @param {event} e Input amount
     */
    _setAmount: function(e) {
      var inputAmount = e.detail;

      if (typeof inputAmount !== 'number') {
        inputAmount = null;
      }

      this.set('setAmount', inputAmount);
    },

    /**
     * Observes if button step change is pressed.
     */
    _onChangePressed: function() {
      this.set('amount', null);
      this.set('_isValid', !!this._amount);
    },

    /**
     * If component is inactive, disables the component (and vice versa).
     * @param {Boolean} isActive Active status
     */
    _activeChanged: function(isActive) {
      var cellsEnterLimitedAmount = this.$.cellsEnterLimitedAmount;
      var inputAmount = cellsEnterLimitedAmount.amount;

      cellsEnterLimitedAmount.set('disabled', !isActive);

      if (inputAmount && isActive) {
        cellsEnterLimitedAmount._checksIfInputIsFilled(inputAmount);
      }
    },

    /**
     * Callback observer for any change on <em>collapsed</em>
     * @param   isCollapsed {Boolean} New value
     * @private
     */
    _onCollapsedChanged: function(isCollapsed) {
      if (isCollapsed && this.isValid() && !this.amount) {
        this.set('amount', this._amount);
      }
    },

    /**
     * Checks whether the user has set a valid input
     * @override
     */
    isValid: function() {
      return this._isValid;
    },

    /**
     * @override
     * @desc resets the component's state
     */
    reset: function() {
      this.set('setAmount', null);
      this.set('amount', null);
      this.set('_amount', null);
      this.set('totalBalance', null);
      this.set('active', false);
      this.set('collapsed', false);
      this.set('_isValid', false);
    },

    /**
     * Behavior when button is pressed.
     * Set amount to filled status.
     * Toggle step.
     * Status active to true.
     * @param {Object} evt Click event
     */
    _onSendAmount: function(evt) {
      var outputAmount = evt.detail;

      this.$.cellsStepEnterAmountFilled.set('amount', outputAmount);
      this.set('amount', outputAmount);
      this.set('_amount', outputAmount);
      this.set('collapsed', true);
      this.set('_isValid', true);
    }
  });
}());
