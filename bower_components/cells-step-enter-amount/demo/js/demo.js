/* global product */
var templateBind = document.getElementById('tbind');
(function(d) {
  'use strict';

  templateBind.addEventListener('dom-change', function() {
  });

  d.addEventListener('WebComponentsReady', function() {
    templateBind.set('product', product);

    var cellsStepEnterAmount = d.getElementById('cellsStepEnterAmount');
    var cellsStepEnterAmount2 = d.getElementById('cellsStepEnterAmount2');
    var cellsStepEnterAmount3 = d.getElementById('cellsStepEnterAmount3');

    // name needs to be informed and unique if there are more than one component
    cellsStepEnterAmount.set('name', 'cellsStepEnterAmount');
    cellsStepEnterAmount.set('totalBalance', 1111.11);
    cellsStepEnterAmount.set('currency', 'EUR');
    cellsStepEnterAmount.set('localCurrency', 'EUR');
    cellsStepEnterAmount.set('language', 'es');
    cellsStepEnterAmount.set('iconCode', 'glomo:GM02');
    cellsStepEnterAmount.set('iconSize', 'icon-size-16');

    // name needs to be informed and unique if there are more than one component
    cellsStepEnterAmount2.set('name', 'cellsStepEnterAmount2');
    cellsStepEnterAmount2.set('totalBalance', 1111.11);
    cellsStepEnterAmount2.set('currency', 'EUR');
    cellsStepEnterAmount2.set('localCurrency', 'EUR');
    cellsStepEnterAmount2.set('language', 'es');
    cellsStepEnterAmount2.set('iconCode', 'glomo:GM02');
    cellsStepEnterAmount2.set('iconSize', 'icon-size-16');
    cellsStepEnterAmount2.set('maxLimit', 100);
    cellsStepEnterAmount2.set('minLimit', 1);

    // name needs to be informed and unique if there are more than one component
    cellsStepEnterAmount3.set('name', 'cellsStepEnterAmount3');
    cellsStepEnterAmount3.set('totalBalance', 1111.11);
    cellsStepEnterAmount3.set('currency', 'EUR');
    cellsStepEnterAmount3.set('localCurrency', 'EUR');
    cellsStepEnterAmount3.set('language', 'es');
    cellsStepEnterAmount3.set('iconCode', 'glomo:GM02');
    cellsStepEnterAmount3.set('iconSize', 'icon-size-16');
  });
}(document));
