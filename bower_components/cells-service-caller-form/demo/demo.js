var serviceForm = document.querySelector('cells-service-caller-form');
serviceForm.set('service', {
  component: 'example-provider-one',
  name: 'service1',
  params: [{
    name: 'param1',
    required: true
  }, {
    name: 'param2',
    required: true
  }, {
    name: 'param3',
    isObject: true
  }
  ],
  requestMethod: 'generateRequest',
  events: ['response', 'error'],
  mock: function() {
    serviceForm.mockComponent.fire('response', 'mocked response');
  }
});
