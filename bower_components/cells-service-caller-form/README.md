# cells-service-caller-form

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

Provides a way to set params for a service and see the response. Intended for demos of data providers and managers.
Its 'service' property takes an object and the cells-service-caller-form will create an instance of your component, store it in its 'component' property, append it in its local DOM for you to inspect. The service object you set must have the following structure:
```
{
  component:
    your component name,
  name:
    the service name,
  params:
    an array of parameters to set to your component.
    Each param is an object with the following structure:
    {
      name:
        the name of the property to set in your component,
      required:
        (optional) true/false, if true it must be filled for the form to work,
      isObject:
        (optional) true/false, if true it must be filled with an object formatted as a JSON string,
      default:
        (optional) default value pre-filled in the form field
    },
  requestMethod:
    the method to be called on your component to initiate the request ('generateRequest' for DPs, 'onTemplateActive', 'doAccount', ... for DMs),
  events:
    an array of events that your component fires,
  mock:
    A function to be used when mocking a DM.
    If this property is present, a 'Mock' button will appear. The function will run when you press it.
    The first time you press the 'Mock' button an instance of your component will be created, stored in the 'mockComponent' property on cells-service-caller-form, and appended it in its local DOM.
    You may use this instance to set up mocks.
    You may want to create a mock file and overwrite your component's DP's _computeURL and _getRequestProperties so you can GET the mock file (which will preserve the DP's regular flow).
    You may instead overwrite your DP's generateRequest method to fire a 'response' event with a mock payload (if your DM listens to its DP's event); or have the generateRequest method return a fulfilled promise with a mock payload; or set your DP's `last-response` property with a mock value (if your DM observes `last-response`).

}
```
Example (DM):
```html
<cells-service-caller-form></cells-service-caller-form>
<script>
document.addEventListener('WebComponentsReady', function() {
    var serviceForm = document.querySelector('cells-service-caller-form');
    serviceForm.set('service', {
      component: "my-dm",
      name: "my-dm",
      params: [
        {name: 'param1',
        required: true
      }, {
        name: 'param2',
        isObject: true
      }
      ],
      requestMethod: 'onTemplateActive',
      events: ['my-dm-event1'],
      mock: function() {
        serviceForm.mockComponent.$.dp.generateRequest = function() {
          this.fire('response', mocks.response);
        }
        serviceForm.mockComponent.onTemplateActive();
      }
    });
  });
</script>
```

Example (DP):
```html
<cells-service-caller-form></cells-service-caller-form>
<script>
document.addEventListener('WebComponentsReady', function() {
    var serviceForm = document.querySelector('cells-service-caller-form');
    serviceForm.set('service', {
      component: "my-dp",
      name: "my-dp",
      params: [
        {name: 'user',
        required: true
      }, {
        name: 'password',
        required: true
      }
      ],
      requestMethod: 'generateRequest',
      events: ['response', 'error']
    });
  });
</script>
```
