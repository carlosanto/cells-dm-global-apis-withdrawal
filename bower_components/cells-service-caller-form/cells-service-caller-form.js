Polymer({
  is: 'cells-service-caller-form',

  properties: {
    /**
     * Encapsulates the service. Format defined below:
     * ```
     *  {
     *  component: "example-component-one",
     *  name: "service1",
     *  params: [{
     *   name: "param1",
     *   required: true,
     *   isObject: true // this field will be parsed as JSON
     *  }, {
     *   name: "param2",
     *   required: true
     *  }],
     *  requestMethod: 'methodName', TODO: Params acceptance
     *   events: ['response', 'error', 'accounts-changed'],
     *   mock: function() {
     *
     *   }
     * }
     * ```
     */
    service: {
      type: Object
    },

    /**
     * Instance of component specified in service object
     */
    component: {
      type: Polymer,
      computed: '_createComponent(service)'
    },

    /**
     * Instance of component to be used of mock calls, created if service object has a mock method
     */
    mockComponent: {
      type: Polymer
    }
  },

  _computePlaceholder: function(isObject) {
    return isObject ? 'Object formatted as a JSON string {"key":"value"}' : '';
  },

  /**
   * Creates component, appends it to cells-service-caller-form registers event listeners.
   * @param {Object} service The JSON info about service we want to call.
   * @return {Polymer} component The just created component
   */
  _createComponent: function(service) {
    var component = document.createElement(service.component);
    service.events.forEach(function(event) {
      component.addEventListener(event, this._displayOutput.bind(this));
    }, this);
    Polymer.dom(this.root).appendChild(component);
    return component;
  },

  /**
   * Show event
   * @param {Event} ev Event fired by the component
   */
  _displayOutput: function(ev) {
    console.log(ev.type, ev);
    var el = this.$$('#' + ev.type);
    el.textContent = JSON.stringify(ev.detail, null, 2);
    this._highlightBackground(el.parentNode);
  },


  /**
   * Set params in component in order to prepare it for request calling
   * @param {Object} ev The event from response
   */
  _setParams: function(params) {
    if (Array.isArray(this.service.params)) {
      var parsedParams = this._parseObjectParams(params);

      Object.keys(parsedParams).forEach(function(key) {
        this.component.set(key, parsedParams[key]);
      }, this);
    }
  },

  /**
   * Finds params with isObject: true and parses them, leaves other params intact
   * @param  {Object} params [Params object from form.serialize()]
   * @return {[type]}        [description]
   */
  _parseObjectParams: function(params) {
    var objectParams = this.service.params.filter(function(param) { //get object-type params
      return param.isObject;
    }).map(function(param) {
      return param.name;
    });
    var result = {}; //set parsed object params
    for (var key in params) {
      if (params.hasOwnProperty(key)) {
        result[key] = objectParams.indexOf(key) !== -1 ?
          this._parseJson(params[key], key) :
          params[key];
      }
    }
    return result;
  },

  /**
   * parses a JSON string or logs an error indicating the name of the param whose input could not be parsed
   * @param  {String} str      String from form input
   * @param  {String} paramKey Label of form input
   * @return {Object}          Object
   */
  _parseJson: function(str, paramKey) {
    var result;
    try {
      result = JSON.parse(str);
    } catch (e) {
      console.log('Param ' + paramKey + ' should be a JSON string');
    }
    return result;
  },

  /**
   * Call service
   */
  _doRequest: function() {
    var form = this.$.form;
    if (form.validate()) {
      this._setParams(form.serialize());
      if (this.service.requestMethod) {
        this.component[this.service.requestMethod]();
      }
    }
  },

  /**
   * Hightlights the background when an event is listened to
   * @param  {[type]} el [description]
   * @return {[type]}    [description]
   */
  _highlightBackground: function(el) {
    el.classList.add('highlightBackground');
    setTimeout(function() {
      el.classList.remove('highlightBackground');
    }, 300);
  },

  /**
   * Creates a component and sets it to mockComponent. Calls the mock method in service object with *this* set to cells-service-caller-form.
   */
  _mock: function() {
    console.log('Mock');
    this.mockComponent = this.mockComponent || this._createComponent(this.service);
    this.service.mock.call(null);
  }
});
