# cells-summary-view

A confirmation showing the operation's total amount and a 'finish' button.

Example:

```html
<cells-summary-view
  active
  amount="342.6"
  fee="{'totalFees':{'amount': 50.00, 'currency': 'EUR'}}"
  currency-code="EUR"
  local-currency="EUR"
  buttonLabel="cells-summary-view-transfer">
</cells-summary-view>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-summary-view-scope      | scope description | default value  |
| --cells-summary-view | empty mixin | {} |
| --cells-summary-view-title | empty mixin | {} |
| --cells-summary-view-quantity | empty mixin | {} |
| --cells-summary-view-comission | empty mixin | {} |
| --cells-summary-view-comission-last | empty mixin | {} |
| --cells-summary-view-legal-terms | empty mixin | {} |
| --cells-summary-view-submit-button | empty mixin | {} |
| --cells-summary-view-legal-terms-button | empty mixin | {} |
