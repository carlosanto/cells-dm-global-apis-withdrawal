/* global productThreeFees, productOneFee, productNoFees, productFeeIsZero */
var templateBind = document.getElementById('tbind');

// The dom-change event signifies when the template has stamped its DOM.
templateBind.addEventListener('dom-change', function() {
  // auto binding template is ready
});

document.addEventListener('WebComponentsReady', function() {
  var summaryViewOneFee = document.getElementById('summaryViewOneFee');
  var summaryViewThreeFee = document.getElementById('summaryViewThreeFee');
  var summaryViewActive = document.getElementById('summaryViewActive');
  var summaryViewWithOneFeeAndAddition = document.getElementById('summaryViewWithOneFeeAndAddition');
  var summaryViewWithThreeFeeAndAddition = document.getElementById('summaryViewWithThreeFeeAndAddition');
  var summaryViewActiveWithFeeZero = document.getElementById('summaryViewActiveWithFeeZero');
  var summaryViewNoActive = document.getElementById('summaryViewNoActive');

  summaryViewOneFee.set('amount', productOneFee.amount);
  summaryViewOneFee.set('currencyCode', productOneFee.currency);
  summaryViewOneFee.set('localCurrency', productOneFee.localCurrency);
  summaryViewOneFee.set('fees', productOneFee.fees);
  summaryViewOneFee.set('buttonLabel', productOneFee.buttonLabel);

  summaryViewThreeFee.set('amount', productThreeFees.amount);
  summaryViewThreeFee.set('currencyCode', productThreeFees.currency);
  summaryViewThreeFee.set('localCurrency', productThreeFees.localCurrency);
  summaryViewThreeFee.set('fees', productThreeFees.fees);
  summaryViewThreeFee.set('buttonLabel', productThreeFees.buttonLabel);

  summaryViewWithOneFeeAndAddition.set('hasAddition', true);
  summaryViewWithOneFeeAndAddition.set('amount', productOneFee.amount);
  summaryViewWithOneFeeAndAddition.set('currencyCode', productOneFee.currency);
  summaryViewWithOneFeeAndAddition.set('localCurrency', productOneFee.localCurrency);
  summaryViewWithOneFeeAndAddition.set('fees', productOneFee.fees);
  summaryViewWithOneFeeAndAddition.set('buttonLabel', productOneFee.buttonLabel);

  summaryViewWithThreeFeeAndAddition.set('hasAddition', true);
  summaryViewWithThreeFeeAndAddition.set('amount', productThreeFees.amount);
  summaryViewWithThreeFeeAndAddition.set('currencyCode', productThreeFees.currency);
  summaryViewWithThreeFeeAndAddition.set('localCurrency', productThreeFees.localCurrency);
  summaryViewWithThreeFeeAndAddition.set('fees', productThreeFees.fees);
  summaryViewWithThreeFeeAndAddition.set('buttonLabel', productThreeFees.buttonLabel);

  summaryViewActiveWithFeeZero.set('amount', productFeeIsZero .amount);
  summaryViewActiveWithFeeZero.set('currencyCode', productFeeIsZero .currency);
  summaryViewActiveWithFeeZero.set('localCurrency', productFeeIsZero.localCurrency);
  summaryViewActiveWithFeeZero.set('fees', productFeeIsZero.fees);
  summaryViewActiveWithFeeZero.set('buttonLabel', productFeeIsZero.buttonLabel);

  summaryViewActive.set('buttonLabel', productNoFees.buttonLabel);
  summaryViewActive.set('fees', productNoFees.fees);

  summaryViewNoActive.set('buttonLabel', productThreeFees.buttonLabel);
  summaryViewNoActive.set('active', false);
});

templateBind.toggleInactive = function() {
  var summaryViewNoActive = document.getElementById('summaryViewNoActive');
  summaryViewNoActive.set('active', !summaryViewNoActive.get('active'));
};
