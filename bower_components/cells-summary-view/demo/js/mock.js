var productThreeFees = {
  currency: 'EUR',
  localCurrency: 'EUR',
  fees: {
    totalFees: {
      amount: 6.66,
      currency: 'EUR'
    },
    itemizeFees: [
      {
        feeType: 'DIRECT_MONTHLY',
        name: 'Direct monthly',
        amount: {
          amount: 1.11,
          currency: 'EUR'
        },
        percentage: 337739178,
        whoPayFee: {
          id: 'SENDER',
          name: 'Sender'
        }
      },
      {
        feeType: 'DIRECT_PERIODIC',
        name: 'Direct periodic',
        amount: {
          amount: 2.22,
          currency: 'EUR'
        },
        percentage: 199417270,
        whoPayFee: {
          id: 'BOTH',
          name: 'Both'
        }
      },
      {
        feeType: 'SPECIFIC_TRANSACTION',
        name: 'Specific transaction',
        amount: {
          amount: 3.33,
          currency: 'EUR'
        },
        percentage: 251651457,
        whoPayFee: {
          id: 'INTERMEDIARY_BANK',
          name: 'Intermediary bank'
        }
      }
    ]
  },
  amount: 1000.10,
  buttonLabel: 'cells-summary-view-transfer'
};

var productOneFee = {
  currency: 'EUR',
  localCurrency: 'EUR',
  fees: {
    totalFees: {
      amount: 1.11,
      currency: 'EUR'
    },
    itemizeFees: [
      {
        feeType: 'DIRECT_MONTHLY',
        name: 'Direct monthly',
        amount: {
          amount: 1.11,
          currency: 'EUR'
        },
        percentage: 337739178,
        whoPayFee: {
          id: 'SENDER',
          name: 'Sender'
        }
      }
    ]
  },
  amount: 1000.10,
  buttonLabel: 'cells-summary-view-transfer'
};

var productFeeIsZero = {
  currency: 'EUR',
  localCurrency: 'EUR',
  fees: {
    totalFees: {
      amount: 0,
      currency: 'EUR'
    },
    itemizeFees: [
      {
        feeType: 'DIRECT_MONTHLY',
        name: 'Direct monthly',
        amount: {
          amount: 0,
          currency: 'EUR'
        },
        percentage: 0,
        whoPayFee: {
          id: 'SENDER',
          name: 'Sender'
        }
      }
    ]
  },
  amount: 1000.10,
  buttonLabel: 'cells-summary-view-transfer'
};

var productNoFees = {
  currency: 'EUR',
  localCurrency: 'EUR',
  fees: {},
  amount: 1000.10,
  buttonLabel: 'cells-summary-view-transfer'
};

var productOneFeeNoTotal = {
  currency: 'EUR',
  localCurrency: 'EUR',
  fees: {
    totalFees: {},
    itemizeFees: [
      {
        feeType: 'DIRECT_MONTHLY',
        name: 'Direct monthly',
        amount: {
          amount: 1.11,
          currency: 'EUR'
        },
        percentage: 337739178,
        whoPayFee: {
          id: 'SENDER',
          name: 'Sender'
        }
      }
    ]
  },
  amount: 1000.10,
  buttonLabel: 'cells-summary-view-transfer'
};