(function() {

  'use strict';

  Polymer({

    is: 'cells-summary-view',

    behaviors: [
      CellsBehaviors.i18nBehavior,
      CellsBehaviors.StepPropBehavior
    ],

    properties: {
      /**
       * Fees for the current transaction
       * @type {Object}
       */
      fees: {
        type: Object,
        observer: '_feesChanged'
      },

      /**
       * Addition of the amount of total fees
       * @type {Number}
       */
      _totalFees: Number,

      /**
       * Array of fees for the current transaction
       * @type {Array}
       */
      _itemizeFees: Array,

      /**
       * the transaction's amount
       * @type {Number}
       */
      amount: Number,

      /**
       * Amount formatted for amount component
       * @type {Number}
       * @private
       */
      _amount: Number,

      /**
       * ISO 4217 code for the currency
       * @type {String}
       */
      currencyCode: String,

      /**
       * ISO 4217 code for the local currency
       * @type {String}
       */
      localCurrency: String,

      /**
       * translation key for the button label
       * @type {String}
       * @example "cells-summary-view-finish"
       */
      buttonLabel: String,

      /**
       * @type {String}
       * @private
       */
      _lang: String,

      /**
       * Control who performs addition of fee and amount.
       * @type {Boolean}
       */
      hasAddition: {
        type: Boolean,
        value: false
      },

      active: {
        type: Boolean,
        value: false
      }
    },

    listeners: {
      'active-changed': '_activeChanged'
    },

    observers: [
      '_getOperationTotal(fees, amount, hasAddition)'
    ],

    attached: function() {
      this._lang = window.I18nMsg.lang; //taken from i18nBehavior
      this._activeChanged(this.active);
    },

    /**
     * override  so that the register event is not fired
     * @override
     */
    initialize: function() {
      return;
    },

    /**
     * Sets _itemizeFees and _totalFees when fees object changes
     * @param {Object} fees Fees
     */
    _feesChanged: function(fees) {
      if (fees) {
        this.set('_itemizeFees', fees.itemizeFees);

        if (fees.totalFees && fees.totalFees.amount) {
          this.set('_totalFees', fees.totalFees.amount);
        }
      }
    },

    /**
     * 'active' listener to hide/show the component
     * @return {Boolean} isActive If component is active
     * @private
     */
    _activeChanged: function(value) {
      if (value instanceof Event) {
        value = value.detail;
        if (typeof value === 'object' && value.hasOwnProperty('value')) {
          value = value.value;
        }
      }
      this.$.collapser.opened = value;
    },

    /**
     * Addition of fees and amount
     * @param {Number} fee Fee
     * @param {Number} amount Amount
     * @return {Number} amount Addition of amount and fees
     * @private
     */
    _getAmountWithFee: function(amount, fees) {
      return amount + fees;
    },

    /**
     * Sets final amount for amount component
     * @param {Number} amount Amount
     * @private
     */
    _setAmount: function(amount) {
      this.set('_amount', amount);
    },

    /**
     * Controls the visibility of the total operation
     * @param {Boolean} isVisible If is visible
     * @private
     */
    _visibilityOperationTotal: function(isVisible) {
      this.$.operationTotal.hidden = isVisible;
    },

    /**
     * Calculates the total amount, considering any fees present, and his visibility
     * @private
     */
    _getOperationTotal: function() {
      var amount = this.amount;
      var feesItems = this._itemizeFees;

      if (feesItems && amount) {
        if (this.hasAddition) {
          amount = this._getAddition(amount, feesItems);
        }

        this._setAmount(amount);
        this._visibilityOperationTotal(false);
      } else {
        this._visibilityOperationTotal(true);
      }
    },

    /**
     * Returns the addition of fees and amount.
     * If there is not a total, gets the first element
     * @param {Number} amount Total amount
     * @param {Object} feesItems Fees items
     * @return {Number} amount Total amount with fees
     * @private
     */
    _getAddition: function(amount, feesItems) {
      var totalFees = this._totalFees;

      if (!totalFees) {
        totalFees = feesItems[0].amount.amount;
      }

      return this._getAmountWithFee(amount, totalFees);
    },

    /**
     * button press listener. Fires an event
     * @private
     */
    _onButtonPressed: function() {
      this.fire('summary-closed');
    }

    /**
     * fired when the confirm button is pressed.
     * @event summary-closed
     */
  });
}());
