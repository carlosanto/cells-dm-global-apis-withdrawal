var templateBind = document.getElementById('tbind');

var amount = 243.40;
var currency = 'EUR';
var localCurrency = 'EUR';

var operations = [
  {
    label: 'Compartir',
    id: 'share',
    icon: 'glomo:share'
  },
  {
    label: 'Descargar recibo',
    id: 'download',
    icon: 'glomo:document'
  }
];

var headerInfo = [
  {
    label: 'Clave de rastreo',
    data: '23456'
  },
  {
    label: 'Referencia',
    data: '245666'
  }
];

var date = new Date();

// The dom-change event signifies when the template has stamped its DOM.
templateBind.addEventListener('dom-change', function() {
  // auto binding template is ready
  templateBind.set('amount', amount);
  templateBind.set('currency', currency);
  templateBind.set('localCurrency', localCurrency);
  templateBind.set('operations', operations);
  templateBind.set('headerInfo', headerInfo);
  templateBind.set('date', date);
});
