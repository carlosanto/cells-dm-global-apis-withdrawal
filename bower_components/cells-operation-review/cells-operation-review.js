(function() {
  /*global moment*/

  'use strict';

  Polymer({

    is: 'cells-operation-review',

    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    properties: {

      /**
       * @type {String}
       * @desc Contains the amount to display in the header
       */
      amount: String,

      /**
       * @type {String}
       * @desc Contains the ISO currency code for the amount
       */
      currency: String,

      /**
       * @type {String}
       * @desc Contains the local ISO currency code for the amount
       */
      localCurrency: String,

      /**
       * @desc full ISO date
       * @type {String}
       */
      date: String,

      /**
       * @type {String}
       * @desc Date to display in the header
       */
      _date: String,

      /**
       * @type {String}
       * @desc time to display in the header
       */
      _time: String,


      /**
       * @type {String}
       * @desc Contains the transaction's origin name to display
       */
      origin: String,

      /**
       * @type {String}
       * @desc Contains the transaction's origin number to display
       */
      originNumber: String,

      /**
       * @type {String}
       * @desc Contains the transaction's destination name to display
       */
      destination: String,

      /**
       * @type {String}
       * @desc Contains the transaction's destination number to display
       */
      destinationNumber: String,

      /**
       * @type {Array[String]}
       * @desc contains any additional information to display in the header
       */
      additionalInfo: Array,

      /**
       * @type {Array[Object]}
       * @desc array containing the operations to display
       */
      operations: {
        type: Array,
        obsever: '_operationsOBserver'
      }
    },

    observers: [
      '_dateOserver(date)'
    ],

    _dateOserver: function(date) {
      date = moment(date);
      this._time = date.format('LT');
      this._date = date.format('LL');
    },

    _newOperative: function(e) {
      this.fire('new-operation');
    },

    _onClose: function() {
      this.fire('close-operation');
    }

    /**
     * @event new-operation
     * @desc fired when the user clicks on the 'new operation' link
     */

    /**
     * @event close-operation
     * @desc fired when the close button button is pressed
     */
  });

}());
