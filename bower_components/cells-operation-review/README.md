# cells-operation-review

Displays a review of a finished operation

Example:
```html
<cells-operation-review
    amount="[[amount]]"
    operations="[[operations]]"
    additional-info="[[headerInfo]]"
    date="3 de Junio, 1992"
    time="16:34"
    origin="Juan Sin Tierra"
    origin-number="1234"
    destination="Cuenta Crédito"
    destination-number="2243">
</cells-operation-review>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-operation-review-scope      | scope description | default value  |
| --cells-operation-review  | empty mixin     | {}             |
| --cells-operation-review | empty mixin | {} |
| --cells-operation-review-text-banner | empty mixin | {} |
| --cells-operation-review-from-to | empty mixin | {} |
| --cells-operation-review-operations-list | empty mixin | {} |
| --cells-operation-review-close | empty mixin | {} |
| --cells-operation-review-new-transfer | empty mixin | {} |
