# cells-ajax-behavior

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

  Interface to make requests to services using cells-ajax.
  Can be used in three different forms:
  - databinding
  - react to events
  - continuation logic

  **To use *databinding*:**

  Observe `lastResponse` and `lastError` to handle the request. More information can be found in the `lastRequest` property.


  **To use *events*:**

  Listen to **response** and **error** events, **request-in-progress** as well.

  The payload for *response* is the response body from the server.
  The payload for *error* is an error object with `message` and `stack` keys. You might want to inspect *lastRequest.xhr.response* for more information and to get the response body from the server.
  Finally, *request-in-progress* has not payload but may be useful for synchronizing.


  **To use *continuation logic*:**

  Using the promise world, the `generateRequest` method gives you back a promise so when the request is done you can specified your logic the way you want, no restrictions apply.
