# cells-text-banner

Your component description.

Example:
```html
<cells-text-banner></cells-text-banner>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-text-banner-scope      | scope description | default value  |
| --cells-text-banner | empty mixin | {} |
| --cells-text-banner-date | empty mixin | {} |
| --cells-text-banner-date-day | empty mixin | {} |
| --cells-text-banner-date-hour | empty mixin | {} |
| --cells-text-banner-amount | empty mixin | {} |
| --cells-text-banner-transaction | empty mixin | {} |
| --cells-text-banner-transaction-number | empty mixin | {} |
