(function() {

  'use strict';

  Polymer({

    is: 'cells-text-banner',

    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    properties: {
      /**
       * @type {String}
       * @desc Date string
       * @example "3 Noviembre, 2015"
       */
      date: String,

      /**
       * @type {String}
       * @desc Time string
       * @example "13:45"
       */
      time: String,

      /**
       * @type {Number}
       * @desc TRansaction's amount
       * @example 3000
       */
      amount: Number,

      /**
       * @type {String}
       * @desc Local currency in ISO format
       * @example "EUR"
       */
      localCurrency: String,

      /**
       * @type {String}
       * @desc Currency in ISO format
       * @example "EUR"
       */
      currency: String
    }

  });

}());
