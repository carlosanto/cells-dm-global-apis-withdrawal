var templateBind = document.getElementById('tbind');

// The dom-change event signifies when the template has stamped its DOM.
templateBind.addEventListener('dom-change', function() {

  // auto binding template is ready
  document.querySelector('paper-tabs').addEventListener('click', function(event) {
    var dataKey = event.target.parentElement.dataset.key;

    switch (dataKey) {
      case '1':
      default:
        templateBind.set('hideBoxShadow', false);
        templateBind.set('hideAnimationLandscape', false);
        break;
      case '2':
        templateBind.set('hideBoxShadow', true);
        templateBind.set('hideAnimationLandscape', false);
        break;
      case '3':
        templateBind.set('hideBoxShadow', false);
        templateBind.set('hideAnimationLandscape', true);
        break;
    }
  });
});
