(function() {

  'use strict';

  Polymer({

    is: 'cells-skeleton-custom',

    properties: {

      /**
       * hide the animation landscape
       * @type {Boolean}
       */
      hideAnimationLandscape: {
        type: Boolean,
        value: false,
        observer: '_hideAnimationLandscapeObserver'
      },

      /**
       * hide the box shadow and margin
       * @type {Boolean}
       */
      hideBoxShadow: {
        type: Boolean,
        value: false,
        observer: '_hideBoxShadowObserver'
      }
    },

    _hideAnimationLandscapeObserver: function(hide) {
      this.toggleClass('hide-element', hide, this.$.animateLayer);
    },

    _hideBoxShadowObserver: function(hideShadow) {
      if (hideShadow) {
        this.customStyle['--cells-skeleton-custom-content'] = 'box-shadow: none; margin: 0;';
      } else {
        this.customStyle['--cells-skeleton-custom-content'] = 'box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.15); margin: 0.625rem;';
      }

      this.updateStyles();
    }


  });

}());
