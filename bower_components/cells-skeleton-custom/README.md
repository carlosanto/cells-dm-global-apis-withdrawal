# cells-skeleton-custom

`<cells-skeleton-custom>` Provides a custom skeleton. HTML structure is fixed from the call element, using the `item-mask` attr for the element that you like to paint.

If you need some special skeleton item, you only create and add to custom skeleton.

Example:

```html
<cells-skeleton-custom>
  <cells-skeleton-item-account></cells-skeleton-item-account>
</cells-skeleton-custom>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property                             | Description                 | Default                                                                                     |
|:--------------------------------------------|:----------------------------|:-------------------------------------------------------------------------------------------:|
| --cells-skeleton-custom-scope               | scope description           | default value                                                                               |
| --cells-skeleton-custom-color-mask          | mask color                  | var(--bbva-200)                                                                             |
| --cells-skeleton-custom-color-bg            | background color            | var(--bbva-white)                                                                           |
| --cells-skeleton-custom-animate-lanscape    | landscape animation mixin   | animation: skAnimationLandscape 2s ease-in-out 1.3s infinite;                               |
| --cells-skeleton-custom                     | empty mixin                 | {}                                                                                          |
| --cells-skeleton-custom-item-mask           | empty mixin                 | {}                                                                                          |
| --cells-skeleton-custom-animate-layer       | empty mixin                 | {}                                                                                          |
| --cells-skeleton-custom-content             | empty mixin                 | {}                                                                                          |

@demo demo/index.html
