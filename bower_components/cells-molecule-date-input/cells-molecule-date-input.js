(function() {

  Polymer({

    is: 'cells-molecule-date-input',

    properties: {
      /**
       * Format date selected.
       */
      format: {
        type: String,
        value: 'DD/MM/YYYY'
      },

      /**
       * Actual Date.
       */
      date: {
        type: String,
        observer: 'setDate',
        value: function() {
          return Date.now();
        }
      },

      /**
       * Actual input date with locale format.
       */
      showDate: {
        type: String
      },

      /**
       * Minimum date allowed (DD-MM-YYYY or DD/MM/YYYY).
       */
      minDate: {
        type: String,
        observer: 'setMinDate'
      },

      /**
       * Maximum date allowed (DD-MM-YYYY or DD/MM/YYYY).
       */
      maxDate: {
        type: String,
        observer: 'setMaxDate'
      },

      /**
       * Boolean attribute indicating whether the selection of dates in the past is allowed.
       */
      allowPastDate: {
        type: Boolean,
        value: false
      },

      /**
       * Default icon.
       */
      icon: {
        type: String,
        value: 'banking:G27'
      },

      /**
       * string property setting weekend days availability
       */
      availableWeekends: {
        type: Boolean,
        value: false
      },

      /**
       * Boolean disabled select date
       */
      disabled: {
        type: Boolean,
        observer: '_setDisabled'
      },

      /**
       * Flags if current userAgent belongs to iOS platform (iPhone or iPad)
       *
       * @private
       */
      _isIOS: {
        type: Boolean,
        value: false
      },

      /**
       * Last date selected
       *
       * @private
       */
      _lastDate: {
        type: String
      },
      /**
       * Used to trigger an observer when changing values
       */
      _fieldValue: {
        type: String,
        value: ''
      }
    },

    ready: function() {

      //Eval value of <em>_isIOS</em>
      [
        /iphone/i,
        /ipad/i
      ].some(function(pattern) {
        this.set('_isIOS', pattern.test(navigator.userAgent));
        return this._isIOS;
      }, this);

      //Attach input callback based on <em>_isIOS</em>
      this.$.field[(this._isIOS ? 'onblur' : 'onchange')] = this._setValue.bind(this);
    },

    /**
     * Reset state
     */
    reset: function() {
      this.setDate(Date.now());
      this.classList.remove('error');
    },

    /**
     * Set the input date value.
     * @param {Date} date The date to set.
     */
    setDate: function(date) {
      this._fieldValue = this.$.lib.time(date).format('YYYY-MM-DD');
      this.set('_lastDate', this.$.lib.time(date).format('YYYY-MM-DD'));
      this.set('showDate', this._formatDate(date));
    },

    /**
     * Set the minimum input date allowed.
     * @param {Date} date The minimum date.
     */
    setMinDate: function(date) {
      this.set('minDate', this.$.lib.time(date).format('YYYY-MM-DD'));
    },

    /**
     * Set the maximum input date allowed.
     * @param {Date} date The maximum date.
     */
    setMaxDate: function(date) {
      this.set('maxDate', this.$.lib.time(date).format('YYYY-MM-DD'));
    },

    /**
     * Get the input date value with format.
     * @param {String} format The format to apply.
     * @return {String} The date value formated.
     */
    getFormatDate: function(format) {
      return this.$.lib.time(this.showDate, this.format).format(format);
    },

    /**
    * Fired after clicking the date input
    * @event input-date-clicked
    */
    _onClick: function(e) {
      if (this.disabled) {
        e.preventDefault();
      } else {
        this.fire('input-date-clicked');
      }
    },

    /**
     * Choose format in function lang.
     * @param {String} date to apply.
     * @return {String} The date value formated.
     */
    _formatDate: function(date) {
      return this.$.lib.time(date).format(this.format);
    },

    /**
     * Set weekends availability
     * @param available {Boolean} Weekends are available.
     */
    setWeekendsAvailability: function(available) {
      this.set('availableWeekends', Boolean(available));
    },

    /**
     * Set class disabled.
     */
    _setDisabled: function() {
      if (this.disabled) {
        this.$.inputDate.classList.add('disabled');
      } else {
        this.$.inputDate.classList.remove('disabled');
      }
    },

    /**
     * Set the input date value with format.
     */
    _setValue: function() {
      var date = this._fieldValue === '' ? this._lastDate : this._fieldValue;
      var today = this.$.lib.time().format('YYYY-MM-DD');
      var isToday = this.$.lib.time(today).isSame(date);
      var isPastDate = this.$.lib.time(today).isAfter(date);
      var selectedUTCDay = new Date(date).getUTCDay();
      this.set('showDate', this._formatDate(date));

      if (!isToday && isPastDate && !this.allowPastDate) {
        this.classList.add('error');
        /**
        * Fired when we select a past date and it isn't allowed
        * @event input-date-error
        */
        this.fire('input-date-error', {
          message: 'You have to select a date bigger than today.',
          error: 'past-date-not-allowed'
        });
      } else if (!this.availableWeekends && (selectedUTCDay === 0 || selectedUTCDay === 6)) {
        this.classList.add('error');
        this.fire('input-date-error', {
          message: 'Weekends not allowed',
          error: 'weekends-not-allowed'
        });
      } else {
        this.classList.remove('error');
        this.set('_lastDate', this.$.lib.time(date).format('YYYY-MM-DD'));
        /**
        * Fired when a valid date is selected.
        * @event input-date-changed
        * @param {{date: (Date)}} set to date selected
        */
        this.fire('input-date-changed', {
          date: date,
          formatDate: this.getFormatDate(this.format)
        });
      }
    }

  });
}());

