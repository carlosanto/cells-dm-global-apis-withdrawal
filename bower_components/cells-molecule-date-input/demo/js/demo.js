/* global moment */

var selectLang = document.querySelector('select');

var actualKey = null;

document.addEventListener('input-date-error', function(e) {
  alert(e.detail.message);
});

var mockNormal = {};

var mockBlockOld = {
  allowPastDate: true
};

var mockDefinedDate = {
  date: '2034-05-15'
};

var mocks = {
  'mockNormal': mockNormal,
  'mockBlockOld': mockBlockOld,
  'mockDefinedDate': mockDefinedDate
};

function setMock(key) {

  actualKey = key || 'mockNormal';

  var contentBox = document.querySelector('#' + actualKey);

  var dateinput = document.createElement('cells-molecule-date-input');
  for (var property in mocks[actualKey]) {
    if (mocks[actualKey].hasOwnProperty(property)) {
      if (property === 'date') {
        dateinput.setDate(mocks[actualKey][property]);
      } else {
        dateinput.set(property, mocks[actualKey][property]);
      }
    }
  }
  document.querySelector('#mockBox').innerHTML = '';
  document.querySelector('#mockBox').appendChild(dateinput);

}

function getFormatedDate() {
  var format = document.querySelector('#format-input').value;
  var result = document.querySelector('cells-molecule-date-input').getFormatDate(format);
  alert(result);
}

function setMinimumDate() {
  var minDate = document.querySelector('#min-date-input').value;
  document.querySelector('cells-molecule-date-input').setMinDate(moment(minDate, 'DD/MM/YYYY').toDate());
}

function setMaximumDate() {
  var minDate = document.querySelector('#max-date-input').value;
  document.querySelector('cells-molecule-date-input').setMaxDate(moment(minDate, 'DD/MM/YYYY').toDate());
}

function setUnavailableWeekends() {
  document.querySelector('cells-molecule-date-input').setWeekendsAvailability(false);
}

function setAvailableWeekends() {
  document.querySelector('cells-molecule-date-input').setWeekendsAvailability(true);
}

function setReset() {
  document.querySelector('cells-molecule-date-input').reset();
}

var myEl;
document.addEventListener('WebComponentsReady', function() {
  myEl = document.querySelector('cells-molecule-date-input');
});

function toggleDisabled() {
  myEl.set('disabled', !myEl.disabled);
}

setMock();
