# cells-molecule-date-input

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

This component draws a date input where you can select a date. It uses the
native datepicker.

The component has a `date` property which contains the actual input value.

Example:

```html
<cells-molecule-date-input></cells-molecule-date-input>
```

### Block past dates

If you want to block past dates, you have to change `allow-past-date`
property to `false`

### Set minimum date

If you want to set a minimum date, you have to set `min-date`
property to `YYYY-MM-DD`, `MM-DD-YYYY`, `MM-DD-YYYY` date

### Set maximum date

If you want to set a maximum date, you have to set `max-date`
property to `YYYY-MM-DD`, `MM-DD-YYYY`, `MM-DD-YYYY` date

## Set property format, by default 'DD/MM/YYYY' (Spanish).

If you want to set a format, you have to set `format`
property to `MM/DD/YYYY`

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|-----------------|-----------------|----------------|
| --date-input-wrapper      | empty mixin for your own styles on date-input element     |     |
| --cells-molecule-date-input     | empty mixin for your own styles on :host     | {} |
| --cells-molecule-date-input-native           | empty mixin for .date__input | {} |
| --cells-molecule-date-input-disabled-before  | empty mixin for .date.disabled:before | {} |
| --cells-molecule-date-input-disabled         | empty mixin for .date.disabled | {} |
| --cells-molecule-date-input-disabled-bg-color | empty mixin for .date.disabled background color | #fff |
| --cells-molecule-date-input-icon | empty muxin for .date__icon | {} |
