var templateBind = document.getElementById('tbind');

// The dom-change event signifies when the template has stamped its DOM.
templateBind.addEventListener('dom-change', function() {
  // auto binding template is ready
  templateBind.set('greeting', 'Try declarative!');

  templateBind.set('windowPixelRatio', window.devicePixelRatio);
});

document.addEventListener('WebComponentsReady', function() {
  templateBind.set('componentScaleValue', document.querySelector('cells-image-overlay-ccds-wrapper').scale);
});
