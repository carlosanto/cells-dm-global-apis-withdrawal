Polymer({

  is: 'cells-molecule-mask',

  properties: {

    /**
    * Number to be masked
    * @type {String}
    */
    value: {
      type: String
    },

    /**
    * Chars with to mask the number
    * @type {String}
    */
    maskChars: {
      type: String,
      value: '*'
    },

    /**
    * Amount of chars of 'value' NOT be masked
    * @type {Number}
    */
    visibleChars: {
      type: Number,
      value: 4
    },

    /**
    * Icon (code) to display when number is masked
    * @type {String}
    */
    iconMasked: {
      type: String,
      observer: '_iconMaskedObserver'
    },

    /**
    * Icon (code) to display when number is NOT masked
    * @type {String}
    */
    iconNotMasked: {
      type: String,
      value: ''
    },

    /**
    * Icon size (8 to 64, only even numbers)
    * @type {String}
    */
    iconSize: {
      type: Number,
      value: 8
    },

    /**
    * If true, then value is masked
    * @type {Boolean}
    */
    masked: {
      type: Boolean,
      value: false
    },

    /**
    * Icons only displayed if both icons defined (not falsy)
    * @type {Boolean}
    */
    _iconHidden: {
      type: Boolean
    },

    /**
    * Handles if the icon should be displayed or not (true if displayed)
    * @type {String}
    */
    _displayedIcon: {
      type: String,
      computed: '_displayedIconComputed(masked, iconMasked, iconNotMasked)'
    },

    /**
    * Displayed number
    * @private
    * @type {String}
    */
    _displayedValue: {
      type: String,
      computed: '_displayedValueComputed(masked, maskChars, visibleChars, value)'
    },

    /**
     * Displayed mask chars
     * @private
     * @type {String}
     */
    _displayedMask: {
      type: String,
      value: function() {
        return '';
      }
    }

  },

  _displayedIconComputed: function(masked, iconMasked, iconNotMasked) {
    return masked ? iconMasked : iconNotMasked;
  },

  _displayedValueComputed: function(masked, maskChars, visibleChars, value) {
    var displayedValue;

    if (masked) {
      this._iconHidden = !this.iconMasked;
      displayedValue = this._getMaskedValue(value, visibleChars);
      this.set('_displayedMask', maskChars);
    } else {
      this._iconHidden = !this.iconNotMasked;
      this.set('_displayedMask', '');
      displayedValue = value;
    }

    return displayedValue;
  },

  _getMaskedValue: function(value, visibleChars) {
    value = value.toString();
    return value ? (value.substr(value.length - visibleChars)) : '';
  },

  _iconMaskedObserver: function(newValue, oldValue) {
    if (oldValue === undefined) {
      this._iconHidden = !(newValue);
    }

  },

  /**
  * Switches between showing or masking the number
  * @method _onClickIcon
  */
  _onClickIcon: function() {
    this.masked = !this.masked;
    this.fire('cells-molecule-mask-changed', { masked: this.masked });
  }

});
