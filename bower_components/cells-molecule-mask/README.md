# cells-molecule-mask

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

Component which you pass it a text value and it will be masked as 'mask-charsAAAA'
(being 'AAAA' the last four digits of the text and mask-char some custom chars, like · or *).

You can switch between showing the masked or the whole text by clicking on the icon. You
can set the icon's size with an even number between 8 and 64 (8 by default).

Also, you can set the icon to be shown when the number is masked and the icon when it is NOT masked.

No icon will be displayed if not icon is defined.

'masked' defines if the text is masked or not.

Example:

```html
<cells-molecule-mask value="ES54029990012312321" masked mask-chars="#" icon-masked="icon-code-masked" icon-not-masked="icon-code-not-masked" icon-size="8"></cells-molecule-mask>

Last 8 chars won't be masked
<cells-molecule-mask value="ES54029990012312321" visibleChars="8" masked mask-chars="#" icon-masked="icon-code-masked" icon-not-masked="icon-code-not-masked" icon-size="8"></cells-molecule-mask>

Initially not masked
<cells-molecule-mask number="ES54029990012312321" mask-chars="***" icon-masked="icon-code-masked" icon-not-masked="icon-code-not-masked"></cells-molecule-mask>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:---------------:|:---------------:| :-------------:|
| --cells-molecule-mask  | empty mixin     | {}             |
| --cells-molecule-mask-separator-font-size  | font-size from value separator | {rem(10px)} |
| --cells-molecule-mask-margin-right  | space in the right of the separator | {rem(3px)} |
| --cells-molecule-mask-top-position  | relative top space of the separator | {rem(-1px)} |
| --cells-molecule-mask-number-font-size  | font-size of the masked number | {rem(14px)} |
