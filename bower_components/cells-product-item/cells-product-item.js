Polymer({

  is: 'cells-product-item',

  properties: {

    /**
    * Name of the product.
    * @type {String}
    */
    name: String,

    /**
    * Description that appears under the name.
    * Include {masked: true} in the object if you want to mask it.
    * @type {Object}
    * @default {masked: true}
    */
    description: {
      type: Object,
      value: function() {
        return {
          masked: false
        };
      }
    },

    /**
    * Image source URL to that will be displayed.
    * @type {String}
    **/
    imgSrc: {
      type: String
    },

    /**
    * Width of the image.
    * @type {Number}
    * @default 60
    */
    imgWidth: {
      type: Number,
      value: 60
    },

    /**
    * Height of the image.
    * @type {Number}
    * @default 38
    */
    imgHeight: {
      type: Number,
      value: 38
    },

    /**
    * @type {label: String, amount: String, currency: String}
    */
    primaryAmount: {
      type: Object
    },

    /**
    * @type {label: String, amount: String, currency: String}
    */
    secondaryAmount: {
      type: Object
    },

    /**
    * Amount's local currency.
    * @type {String}
    */
    localCurrency: String,

    /**
    * Visible chars on masked strings.
    * @type {Number}
    * @default 4
    */
    visibleChars: {
      type: Number,
      value: 4
    },

    /**
    * If true, an overlay icon (defined by imgOverlayIcon property)
    * will be shown above the image (defined by imgSrc property)
    * @type {Boolean}
    * @default false
    */
    showImgOverlay: {
      type: Boolean,
      value: false
    },

    /**
    * Icon code of the overlay image
    * @type {String}
    */
    imgOverlayIcon: {
      type: String
    },

    /**
    * Icon size of the overlay image
    * @type {String}
    * @default 16
    */
    imgOverlayIconSize: {
      type: Number,
      value: 16
    }

  }
});
