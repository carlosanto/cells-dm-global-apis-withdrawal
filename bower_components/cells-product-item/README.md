# `<cells-product-item>`

`<cells-product-item>` is a component to display a bank product (typically a card or account) with an optional image and description and up to two amounts in a concrete currency and optional label.

The component has two sets of hooks in which you can insert your own HTML (<content> tag).
If your product has a name, then you have 3 hooks: one next to product name (.icons) devised for icons, one at the right of the image (.text-right-image) and one below all the content (.text-below-image),
if name is not found then you have a hook (.description) to display something above the mask number product.


**Example:**

```html
<cells-product-item
  name="My Account"
  img-src="url/to/my/image.png"
  show-img-overlay
  imgo-overlay-icon="glomo:blocked"
  description='{"value": "ES04425233523", "masked": true}'
  primary-amount='{"label": "Available", "amount": 288, "currency": "EUR"}'
  secondary-amount='{"amount": 100, "currency": "USD"}'
  local-currency="EUR">
</cells-product-item>

<cells-product-item
  name="My Account"
  primary-amount='{"label": "Available", "amount": 288, "currency": "EUR"}'
  local-currentcy="cells-product-item-title-color">
  <div class="description">
    <span class="amount-label">Cuenta Simple</span>
    <span class="amount">12345678</span>
  </div>
</cells-product-item>

<cells-product-item>
  <cells-atom-icon class="icons mobile icon-size-20" icon="glomo:mobile"></cells-atom-icon>
</cells-product-item>
```

## Properties

| Name | Description | Default |
|:---------------|:------------|:--------------|
| name (String) | Name of the product | '' |
| description (Object) | Description of the product, can be masked or not => description: {value: 'example', masked: true} | '{masked: false}' |
| imgSrc (String) | URL of the image to display | '' |
| imgWidth (Number) | Width of the image. | 60 |
| imgHeight (Number) | Height of the image. | 38 |
| primaryAmount (Object) | Amount to display with an optional label (see <cells-atom-amount> component if you have doubts of currency) => primaryAmount: {label: 'Available', amount: 288, currency: 'EUR'}| {} |
| secondaryAmount (Object) | Same as primaryAmount. Displayed below of it| {} |
| localCurrency (String) | Amount's local currency.| '' |
| showImgOverlay (Boolean) | If true, an overlay icon (defined by imgOverlayIcon property) will be shown above the image (defined by imgSrc property)| false |
|imgOverlayIcon (String)|  Icon code of the overlay image.||
|imgOverlayIconSize (Number)|Icon size of the overlay image.|16|

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------| :--------------|
| --cells-product-item | empty mixin for the whole component | {} |
| --cells-product-item-image | empty mixin for the image wrapper | {} |
| --cells-product-item-img | empty mixin for the `<img>` element | {} |
| --cells-product-item-name | empty mixin for the product name (title) | {} |
| --cells-product-item-description | empty mixin for the description under the name | {} |
| --cells-product-item-amount-label| empty mixin for the labels over the amounts | {} |
| --cells-product-item-background-color | Background of the component | #fff|
| --cells-product-item-active-background-color | Background of the component on tap | #F4F4F4 |
| --cells-product-item-color | Color of the component | #121212 |
| --cells-product-item-title-icon-color | Text color for disabled status (blocked or frozen) | #D3D3D3 |
