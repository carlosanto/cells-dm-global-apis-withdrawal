var app = document.querySelector('#app');

document.addEventListener('WebComponentsReady', function() {
  var myEl = document.querySelector('#myEl');

  app.set('selected', 0);

  app.set('message', 'cells-atom-message-testing');
  app.set('link', 'cells-atom-message-testing-link');

  app.show = function() {
    app.$['myEl' + app.selected].show();
  };

  app.hide = function() {
    app.$['myEl' + app.selected].hide();
  };
});

document.addEventListener('cells-atom-message-link-clicked', function(e) {
  app.$.toast.text = 'Fired ' + e.type;
  app.$.toast.open();
});
