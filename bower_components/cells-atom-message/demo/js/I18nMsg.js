window.I18nMsg = {};
window.I18nMsg.url = 'locales-demo';

// Demo i18n
var selectLang;
var app;

document.addEventListener('WebComponentsReady', function() {
  app.changeLang = function(e) {
    document.documentElement.lang = e.target.value;
    I18nMsg.lang = e.target.value;
  };
});
