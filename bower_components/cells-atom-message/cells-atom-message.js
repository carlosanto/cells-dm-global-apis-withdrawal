(function() {

  'use strict';

  Polymer({

    is: 'cells-atom-message',

    behaviors: [
      window.CellsBehaviors.i18nBehavior
    ],

    properties: {
      /*
      * Reference of icon, dependence of atom-icon component
      * @type {String}
      * @public
      */
      icon: {
        type: String
      },
      /*
      * Icon size, dependence of atom-icon component
      * @type {String}
      * @public
      */
      iconSize: {
        type: String,
        value: 64
      },
      /**
       * Text to show in the link
       * @type {String}
       */
      link: {
        type: String,
        value: ''
      },
      /*
      * Message to shown
      * @type {String}
      * @public
      */
      message: {
        type: String
      },
      /*
      * Visible element
      * @type {Boolean}
      * @public
      */
      visible: {
        type: Boolean,
        value: true
      },
      /**
       * Type of the message: error, warning, success or info.
       * @type {String}
       */
      type: {
        type: String,
        reflectToAttribute: true
      }
    },
    /*
    * Show the message
    */
    show: function() {
      this.set('visible', true);
    },
    /*
    * Hide the message
    */
    hide: function() {
      this.set('visible', false);
    },
    /*
    * Checked Icon
    */
    _checkedIcon: function(icon) {
      return Boolean(icon);
    },
    /**
     * Fires an event when the link is clicked
     * @param  {Event} e Click event
     * @event cells-atom-message-link-clicked
     */
    _handleLinkClick: function(e) {
      e.preventDefault();
      this.fire('cells-atom-message-link-clicked');
    }
  });
}());
