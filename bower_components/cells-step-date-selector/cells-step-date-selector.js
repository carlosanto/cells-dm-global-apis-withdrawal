(function() {

  'use strict';

  Polymer({

    is: 'cells-step-date-selector',

    behaviors: [
      CellsBehaviors.i18nBehavior,
      CellsBehaviors.StepBehavior
    ],

    properties: {
      /**
       * @type [Object]
       * @desc date selected by the user
       */
      selectedDate: {
        notify: true,
        type: Object
      },

      _selectedDate: {
        observer: '_selectedDateObserver',
        type: String
      },

      _formattedDate: {
        type: String
      }
    },

    listeners: {
      'change-pressed': '_onChangePressed',
      'collapsed-changed': '_onCollapsedChanged'
    },

    observers: [
      '_onActiveChanged(active)'
    ],

    /**
     * @override
     * @desc resets the component's state
     */
    reset: function() {
      this.set('selectedDate', null);
      this.set('active', false);
      this.set('collapsed', false);
      this.set('_selectedDate', null);
      this.set('_formattedDate', '');
      this._cleanSelected();
    },

    /**
     * @override
     * @desc checks whether the user has set a valid input
     */
    isValid: function() {
      return !!this._selectedDate;
    },

    _onActiveChanged: function(active) {
      this.$.datePicker.querySelector('cells-molecule-date-input').disabled = !active;
    },

    _onCollapsedChanged: function(isCollapsed) {
      var value = null;
      if (isCollapsed instanceof Event) {
        isCollapsed = isCollapsed.detail;
      }
      if (isCollapsed && isCollapsed.hasOwnProperty('value')) {
        isCollapsed = isCollapsed.value;
      }
      if (isCollapsed && isCollapsed.hasOwnProperty('data')) {
        isCollapsed = isCollapsed.data;
      }
      if (this.isValid() && isCollapsed === true) {
        value = this._selectedDate;
      }
      if (this.selectedDate !== value) {
        this.set('selectedDate', value);
        this.fire('date-selected', value);
      }
    },

    _selectedDateObserver: function(newDate) {
      if (this.active) {
        this.set('selectedDate', newDate); //@todo: returns a Date object, not the ISO date value
        this.set('_selectedDate', newDate);
        this.fire('date-selected', newDate);

        if (newDate) {
          this.$.stepContainer.toggle();
        }
      }
    },

    _onChangePressed: function() {
      this.set('selectedDate', '');
      this.fire('date-selected', null);
      this._cleanSelected();
    },

    _cleanSelected: function() {
      var items = Array.from(this.$.datePicker.querySelectorAll('.cells-date-selector__item--option'));

      items.forEach(function(elem) {
        elem.classList.remove('selected');
      });
    }

    /**
     * @event date-selected
     * @description fired when a date is selected. Contains the ISO date
     */

  });

}());
