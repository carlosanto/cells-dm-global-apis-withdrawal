# cells-step-date-selector

Date selector step for an operative.

Example:
```html
<cells-step-date-selector
  current-step="3"
  max-steps="5"
  active
  title="SELECCIONA UNA FECHA">
</cells-step-date-selector>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-step-date-selector-scope      | scope description | default value  |
| --cells-step-date-selector  | empty mixin     | {}             |
