Polymer({
  is: 'cells-atom-subheader',
  properties: {
	/**
	* Aria level for the heading
	*/
    headingLevel: {
      type: Number,
      value: 2
    }
  }
});
