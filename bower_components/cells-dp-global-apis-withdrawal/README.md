# cells-dp-global-apis-transfer

Version: **1.0.0**

> CELLS Data Provider for GLOBAL APIS Withdrawal

## Services

## Self API

| Name | Type | Description | Binding |
| --- | --- | --- | --- |
| simulated | Boolean | Flags if operation will be simulated | IN |
| service | String | Service endpoint | IN |
| body | Object | Request body | IN |
