/* global CellsBehaviors */
(function() {

  'use strict';

  Polymer({

    is: 'cells-step-concept',

    behaviors: [
      CellsBehaviors.i18nBehavior,
      CellsBehaviors.StepBehavior
    ],

    properties: {

      /**
       * @type {String}
       */
      concept: {
        type: String,
        notify: true
      },

      /**
       * @type {String}
       * @desc used for certain countries
       */
      reference: {
        type: String,
        notify: true
      },

      showReference: {
        type: Boolean,
        value: false,
        observer: '_showReferenceObserver'
      },

      /**
       * @type {String}
       */
      _concept: String,

      /**
       * @type {String}
       */
      _reference: String
    },

    listeners: {
      'stepContainer.change-pressed': '_onChangePressed'
    },

    isValid: function() {
      return true;
    },

    reset: function() {
      this.set('concept', null);
      this.set('_concept', null);
      this.set('reference', null);
      this.set('_reference', null);
    },

    _showReferenceObserver: function(value) {
      if (value) {
        this._setReference();
      }
    },

    _setReference: function() {
      var lang = window.I18nMsg.lang; //taken from i18nBehavior

      this.set('_reference', new Date().toLocaleDateString(lang || 'es'));
    },

    _onChangePressed: function(e) {

      e.stopPropagation();

      this.set('concept', null);
      this.set('reference', null);

      this.fire('change-pressed', {
        concept: this.concept,
        reference: this.reference
      });
    },

    _onContinue: function() {
      this.set('concept', this._concept);
      this.set('reference', this._reference);

      this.$.stepContainer.toggle();

      this.fire('continue-pressed', {
        concept: this.concept,
        reference: this.reference
      });
    }

    /**
     * @event continue-pressed
     * @desc fired when the continue button is pressed. Contains an object with the concept and reference
     */

    /**
     * @event change-pressed
     * @desc fired when the change button is pressed. Contains an object with the concept and reference
     */

  });

}());
