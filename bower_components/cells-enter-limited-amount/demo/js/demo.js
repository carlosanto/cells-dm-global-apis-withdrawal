/* global product, product2, product3 */
var templateBind = document.getElementById('tbind');

// The dom-change event signifies when the template has stamped its DOM.
templateBind.addEventListener('dom-change', function() {
  // auto binding template is ready

});

document.addEventListener('WebComponentsReady', function() {
  var cellsEnterLimitedAmountEur = document.getElementById('cellsEnterLimitedAmountEur');
  var cellsEnterLimitedAmountNoLimits = document.getElementById('cellsEnterLimitedAmountNoLimits');
  var cellsEnterLimitedAmountUsd = document.getElementById('cellsEnterLimitedAmountUsd');
  var cellsEnterLimitedAmountDisabled = document.getElementById('cellsEnterLimitedAmountDisabled');

  cellsEnterLimitedAmountEur.set('totalAvailable', product.totalAvailable);
  cellsEnterLimitedAmountEur.set('maxLimit', product.maxLimit);
  cellsEnterLimitedAmountEur.set('minLimit', product.minLimit);
  cellsEnterLimitedAmountEur.set('currencyCode', product.currency);
  cellsEnterLimitedAmountEur.set('localCurrency', product.localCurrency);
  cellsEnterLimitedAmountEur.set('language', product.language);
  cellsEnterLimitedAmountEur.set('name', product.name);
  cellsEnterLimitedAmountEur.set('iconCode', product.iconCode);
  cellsEnterLimitedAmountEur.set('iconSize', product.iconSize);


  cellsEnterLimitedAmountNoLimits.set('totalAvailable', product2.totalAvailable);
  cellsEnterLimitedAmountNoLimits.set('currencyCode', product2.currency);
  cellsEnterLimitedAmountNoLimits.set('localCurrency', product2.localCurrency);
  cellsEnterLimitedAmountNoLimits.set('language', product2.language);
  cellsEnterLimitedAmountNoLimits.set('name', product2.name);
  cellsEnterLimitedAmountNoLimits.set('iconCode', product2.iconCode);
  cellsEnterLimitedAmountNoLimits.set('iconSize', product2.iconSize);

  cellsEnterLimitedAmountUsd.set('totalAvailable', product3.totalAvailable);
  cellsEnterLimitedAmountUsd.set('maxLimit', product3.maxLimit);
  cellsEnterLimitedAmountUsd.set('minLimit', product3.minLimit);
  cellsEnterLimitedAmountUsd.set('currencyCode', product3.currency);
  cellsEnterLimitedAmountUsd.set('localCurrency', product3.localCurrency);
  cellsEnterLimitedAmountUsd.set('language', product3.language);
  cellsEnterLimitedAmountUsd.set('name', product3.name);
  cellsEnterLimitedAmountUsd.set('iconCode', product3.iconCode);
  cellsEnterLimitedAmountUsd.set('iconSize', product3.iconSize);

  cellsEnterLimitedAmountDisabled.set('disabled', true);
  cellsEnterLimitedAmountDisabled.set('totalAvailable', product.totalAvailable);
  cellsEnterLimitedAmountDisabled.set('currencyCode', product.currency);
  cellsEnterLimitedAmountDisabled.set('localCurrency', product.localCurrency);
  cellsEnterLimitedAmountDisabled.set('language', product.language);
  cellsEnterLimitedAmountDisabled.set('name', product.name);
  cellsEnterLimitedAmountDisabled.set('iconCode', product.iconCode);
  cellsEnterLimitedAmountDisabled.set('iconSize', product.iconSize);
});