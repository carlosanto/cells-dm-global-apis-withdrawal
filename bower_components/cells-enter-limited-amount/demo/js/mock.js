var product = {
  amount: 1111.11,
  currency: 'EUR',
  localCurrency: 'EUR',
  language: 'es',
  name: 'cellsEnterLimitedAmountEur',
  iconCode: 'glomo:GM02',
  iconSize: 'icon-size-16',
  totalAvailable: 100000.50,
  maxLimit: 10000,
  minLimit: 1
};

var product2 = {
  amount: '1111,11',
  currency: 'EUR',
  localCurrency: 'EUR',
  language: 'es-us',
  name: 'cellsEnterLimitedAmountEurUs',
  iconCode: 'glomo:GM02',
  iconSize: 'icon-size-16',
  totalAvailable: 222222.50,
  maxLimit: 100000,
  minLimit: 0
};

var product3 = {
  amount: 1111.11,
  currency: 'USD',
  localCurrency: 'USD',
  language: 'en',
  name: 'cellsEnterLimitedAmountUsd',
  iconCode: 'glomo:GM02',
  iconSize: 'icon-size-16',
  totalAvailable: 100000.50,
  maxLimit: 10000,
  minLimit: 1
};