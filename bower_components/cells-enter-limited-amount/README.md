# cells-enter-limited-amount

Enter amount component.

Example:
```html
<cells-enter-limited-amount></cells-enter-limited-amount>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-enter-limited-amount  | empty mixin     | {}             |
