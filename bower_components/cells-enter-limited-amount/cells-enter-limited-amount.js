/* global CellsBehaviors */
(function() {

  'use strict';

  Polymer({
    is: 'cells-enter-limited-amount',

    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    properties: {
      /**
       * Amount set in the input from the outside.
       * @type {Number}
       */
      setAmount: {
        type: Number,
        observer: '_setAmountChanged'
      },

      /**
       * Output amount.
       * Not visible
       * @type {Number}
       */
      amount: {
        type: Number,
        observer: '_amountChanged'
      },

      /**
       * Available balance for the operation.
       * Receives amount and currency.
       * @type {Number}
       */
      totalAvailable: Number,

      /**
       * Stores the initial available balance for the operation.
       * @type {String}
       */
      _totalAvailableCalculated: {
        type: String
      },

      /**
       * Name required in amount component
       * Mandadatory and needs to be unique to avoid conflicts with cleave.js
       * @type {String}
       */
      name: {
        type: String,
        value: 'enterAmount'
      },

      /**
       * Language for the currency
       * @type {String}
       */
      language: String,

      /**
       * ISO 4217 code for the currency
       * @type {String}
       */
      currencyCode: String,

      /**
       * ISO 4217 code for the local currency
       * @type {String}
       */
      localCurrency: {
        type: String
      },

      /**
       * The icon code of the icon-set. View documentation for the icon component
       * @type {String}
       */
      iconCode: String,

      /**
       * The icon size of the icon. View documentation for the icon component
       * @type {String}
       */
      iconSize: String,

      /**
       * Control who actives the button when the amount is correct
       * @type {Boolean}
       */
      _buttonIsEnabled: {
        type: Boolean,
        value: false
      },

      /**
       * If true, the element is currently disabled.
       * @type {Boolean}
       */
      disabled: {
        type: Boolean,
        value: false,
        observer: '_disabledChanged'
      },

      /**
       * Max limit for the operation.
       * Optional
       * @type {Number}
       */
      maxLimit: Number,

      /**
       * Min limit for the operation.
       * Optional
       * @type {Number}
       */
      minLimit: Number,

      /**
       * Control who indicates than there are limits.
       * Shows limits.
       * @type {Boolean}
       */
      _withLimits: {
        type: Boolean,
        value: false
      }
    },

    listeners: {
      'amount-input-changed': '_setAmount'
    },

    observers: [
      '_totalAvailableChanged(totalAvailable, currencyCode)',
      '_limitChanged(maxLimit, minLimit)'
    ],

    /**
     * Unlisten when the input changes
     */
    detached: function() {
      this.async(this._asyncTotalAvailable);
    },

    /**
     * Stores the output amount.
     * @param {event} e Input amount
     */
    _setAmount: function(e) {
      var inputAmount = e.detail;

      if (typeof inputAmount !== 'number') {
        inputAmount = undefined;
      }

      this.set('amount', inputAmount);
    },

    /**
     * Observes the initial amount and set the amount in the input.
     * @param {String} amount Entered amount
     */
    _setAmountChanged: function(amount) {
      if (amount) {
        this.$.enterAmountInput.set('setAmount', amount);
      } else {
        this.set('setAmount', undefined);
        this.set('amount', undefined);
      }
    },

    /**
     * Observes when the output amount changes and validate it.
     * Checks for limits.
     * Checks for available balance.
     * @param {String} amount Output amount
     */
    _amountChanged: function(amount) {
      if (this._withLimits) {
        amount = this._validateLimits(amount);

        if (!amount) {
          // console.log('out of limits');
        }
      }

      this._checksIfInputIsFilled(amount);

      if (this.totalAvailable) {
        this._asyncTotalAvailable = this.async(function() {
          this._refreshTotalAvailable(amount);
        }, 500);
      }
    },

    /**
     * Observes when the limits changes.
     * Checks for minLimit and set it.
     * Checks for maxLimit and set it.
     * @param {Object} limits Limits
     */
    _limitChanged: function(maxLimit, minLimit) {
      this.set('minLimit', minLimit);
      this.set('maxLimit', maxLimit);
      this.set('_withLimits', true);
    },

    /**
     * Observes when the available balance changes and stores the initial available balance for internal operations.
     * @param {Object} totalAvailable Available balance
     */
    _totalAvailableChanged: function(totalAvailable, currencyCode) {
      this._refreshTotalAvailable(this.amount);
    },

    /**
     * Observes if input is filled.
     * Enables/disables button.
     * @param {String} amount Entered amount
     */
    _checksIfInputIsFilled: function(amount) {
      this.set('_buttonIsEnabled', (!isNaN(amount) && amount !== 0));
    },

    /**
     * Observes if limits changes and validates it.
     * Enables/disables button.
     * @param {String} amount Entered amount
     */
    _validateLimits: function(amount) {
      if (amount >= this.minLimit && amount <= this.maxLimit) {
        return amount;
      } else {
        return 0;
      }
    },

    /**
     * Sets the available balance when the entered amount is subtracted.
     * @param {String} amountEntered Entered amount
     */
    _refreshTotalAvailable: function(amountEntered) {
      if (amountEntered > 0) {
        var totalAvailableCalculated = (this.totalAvailable - amountEntered);

        this.set('_totalAvailableCalculated', totalAvailableCalculated);
      } else {
        this.set('_totalAvailableCalculated', this.totalAvailable);
      }
    },

    /**
     * Disabled input and button.
     * @param {Boolean} disabled
     */
    _disabledChanged: function(disabled) {
      if (disabled) {
        this.$.enterAmountInput.set('disabled', true);
        this.set('_buttonIsEnabled', false);
      } else {
        this.$.enterAmountInput.set('disabled', false);

        if (this.amount > 0) {
          this.set('_buttonIsEnabled', true);
        }
      }
    },

    /**
     * Sets the available balance when the entered amount is subtracted.
     * @param {String} amountEntered Entered amount
     */
    _sendAmountOutput: function() {
      this.fire('send-amount', this.amount);
    }
  });
}());
