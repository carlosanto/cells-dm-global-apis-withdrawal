

##&lt;cells-icons&gt;

`cells-icons` is a utility import that includes the definition for the `atom-icon` element, `iron-iconset-svg` element, as well as an import for the default icon set.

The `cells-icons` directory also includes imports for additional icon sets that can be loaded into your project.

Example loading icon set:

```html
<link rel="import" href="../cells-icons/cells-icons.html">
```

To use an icon from one of these sets, first prefix your `atom-icon` with the icon set name, followed by a colon, ":", and then the icon id.

Example using the A01 icon from the buzz icon set:

```html
<atom-icon icon="buzz:A01"></atom-icon>
```

See [atom-icon](#atom-icon) for more information about working with icons.

See [iron-iconset](#iron-iconset) and [iron-iconset-svg](#iron-iconset-svg) for more information about how to create a custom iconset.


