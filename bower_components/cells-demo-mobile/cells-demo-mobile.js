Polymer({

  is: 'cells-demo-mobile',

  properties: {
    /**
     * Skin selected in dropdown
     * @type {String}
     */
    selectedSkin: {
      type: String,
      value: 'nexus5',
      observer: '_selectedSkinChanged'
    },

    /**
     * Orientation selected in dropdown
     * @type {String}
     */
    selectedOrientation: {
      type: String,
      value: 'portrait',
      observer: '_selectedOrientationChanged'
    },

    /**
     * Minimum screen width in pixels to show the mobile shell.
     * Set to 0 to always show the shell.
     */
    shellAtMinWidth: {
      type: Number,
      value: 769
    }
  },

  observers: [ '_toggleMobileVisibilityClass(showShell)' ],

  /**
   * Function to change the skin of screen
   * @param {string} elem - The id of the skin.
   * @private
   */
  _selectedSkinChanged: function(elem) {
    if (elem !== '') {
      Polymer.dom(this.root).querySelector('.marvel-device').classList.toggle(this.selectedSkin);

      this.selectedSkin = elem;
      Polymer.dom(this.root).querySelector('.marvel-device').classList.add(this.selectedSkin);
    }

    Polymer.dom(this.root).querySelector('#select-skin').value = elem;
  },

  /**
   * Function to change the orientation of screen
   * @param {string} elem - The id of the orientation. Must be portrait or landscape
   * @private
   */
  _selectedOrientationChanged: function(elem) {
    if (elem !== '') {
      Polymer.dom(this.root).querySelector('.marvel-device').classList.toggle(this.selectedOrientation);

      this.selectedOrientation = elem;
      Polymer.dom(this.root).querySelector('.marvel-device').classList.add(this.selectedOrientation);
    }

    Polymer.dom(this.root).querySelector('#select-orientation').value = elem;
  },

  /**
   * Event of dropdown changed
   * @param {Object} ev - Event launched when an user selects value in dropdown
   * @private
   */
  onDropdownChanged: function(ev) {
    if (ev.target.id === 'select-skin') {
      this._selectedSkinChanged(ev.target.value);
    } else {
      this._selectedOrientationChanged(ev.target.value);
    }
  },

  _toggleMobileVisibilityClass: function(showShell) {
    this._elems = this._elems || Polymer.dom(this.root).querySelectorAll('.js-mobile-unstyled');

    for (var i = 0, len = this._elems.length; i < len; i++) {
      this.toggleClass('mobile-unstyled', !showShell, this._elems[i]);
    }
  }

});
