/* global moment */

(function() {

  'use strict';

  Polymer({

    is: 'cells-date-selector',

    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    properties: {
      /**
       * @type {String}
       * @desc Icon for the 'today' option
       */
      iconToday: {
        type: String,
        value: 'glomo:clock'
      },

      /**
       * @type {String}
       * @desc Icon for the 'other date' option
       */
      iconDate: {
        type: String,
        value: 'glomo:calendar'
      },

      /**
       * @type {String}
       * @desc Icon for the arrow after the text
       */
      iconArrow: {
        type: String,
        value: 'glomo:angle-down'
      },

       /**
       * @type {String}
       * @desc Icon for the arrow after the text
       */
      iconArrowSize: {
        type: Number,
        value: 12
      },

      /**
       * @type {Number}
       * @desc Icon size for the 'today' option
       */
      iconTodaySize: {
        type: Number,
        value: 24
      },

      /**
       * @type {Number}
       * @desc Icon size for the 'other date' option
       */
      iconDateSize: {
        type: Number,
        value: 24
      },

      /**
       * @type {Object}
       * @desc contains the selected date
       */
      selectedDate: {
        type: Object,
        notify: true
      },

      /**
       * @type {String}
       * @desc contains the formatted selected date
       */
      selectedDateFormatted: {
        type: String,
        notify: true
      },

      /**
       * @type {String}
       * @desc minimum date for the input. Set to the current day
       */
      _minDate: String
    },

    attached: function() {
      this.set('_minDate', moment().format('MM-DD-YYYY'));
    },

    _selectDateToday: function(e) {
      //prevent moment event bubble
      e.preventDefault();
      e.stopPropagation();

      this.set('selectedDate', new Date());
      this.set('selectedDateFormatted', moment(this.selectedDate).format('LL'));
    },

    _selectDate: function(e) {
      //prevent event bubble
      e.preventDefault();
      e.stopPropagation();

      var date = moment(e.currentTarget.showDate, 'DD/MM/YYYY');
      if (date.isValid()) {
        this.set('selectedDate', date.toDate());
        this.selectedDateFormatted = date.format('LL');
      }
    }
  });

}());
