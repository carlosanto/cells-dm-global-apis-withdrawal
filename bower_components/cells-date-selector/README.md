# cells-date-selector

`<cells-date-selector>` provides a selector date, today or other date.

Example:

```html
<cells-date-selector icon-today="glomo:clock" icon-date="glomo:calendar" icon-tick="glomo:tick"></cells-date-selector>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property                  | Description       | Default        |
|:---------------------------------|:------------------|:--------------:|
| --cells-date-selector | empty mixin | {} |
| --cells-date-selector-item | empty mixin | {} |
| --cells-date-selector-item-active | empty mixin | {} |
| --cells-date-selector-item-first-child | empty mixin | {} |
| --cells-date-selector-item-option | empty mixin | {} |
| --cells-date-selector-icon | empty mixin | {} |
| --cells-date-selector-icon-text | empty mixin | {} |
| --cells-date-selector-link-icon | empty mixin | {} |
| --cells-date-selector-selected-date | empty mixin | {} |
| --cells-date-selector-item-selected | empty mixin | {} |
| --cells-date-selector-item-caption | empty mixin | {} |
| --cells-date-selector-item-caption-paragraph | empty mixin | {} |
| --cells-date-selector-date-input | empty mixin | {} |
| --cells-date-selector-inactive | empty mixin | {} |
| --cells-date-selector-icon-inactive | empty mixin | {} |
| --cells-date-selector-paragraph-inactive | empty mixin | {} |

@demo demo/index.html
