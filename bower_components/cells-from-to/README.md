# cells-from-to

Your component description.

Example:
```html
<cells-from-to></cells-from-to>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-from-to | empty mixin | {} |
| --cells-from-to-list | empty mixin | {} |
| --cells-from-to-list-item | empty mixin | {} |
| --cells-from-to-list-item-odd | empty mixin | {} |
| --cells-from-to-list-item-odd-after | empty mixin | {} |
| --cells-from-to-initial | empty mixin | {} |
| --cells-from-to-initial-before | empty mixin | {} |
| --cells-from-to-initial-content | empty mixin | {} |
| --cells-from-to-info | empty mixin | {} |
| --cells-from-to-item | empty mixin | {} |
| --cells-from-to-item-odd | empty mixin | {} |
| --cells-from-to-info-name | empty mixin | {} |
| --cells-from-to-molecule-mask-number | empty mixin | {} |
