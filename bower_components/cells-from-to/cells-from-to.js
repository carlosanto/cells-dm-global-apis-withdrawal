(function() {

  'use strict';

  Polymer({

    is: 'cells-from-to',

    properties: {

      /**
       * @type {String}
       * @desc origin to display
       * @example "Simple Account"
       * @example "David"
       */
      origin: String,

      /**
       * @type {String}
       * @desc origin number to display
       * @example "1234"
       */
      originNumber: String,

      /**
       * @type {String}
       * @desc destination to display
       * @example "Simple Account"
       * @example "David"
       */
      destination: String,

      /**
       * @type {String}
       * @desc destination number to display
       * @example "1234"
       */
      destinationNumber: String,

      _iconOrigin: String,

      _iconDestination: String
    },

    observers: [
      '_computeIcons(origin, destination)'
    ],

    _computeIcons: function(origin, destination) {
      this._iconOrigin = origin ? origin[0] : '';
      this._iconDestination = destination ? destination[0] : '';
    }

  });

}());
