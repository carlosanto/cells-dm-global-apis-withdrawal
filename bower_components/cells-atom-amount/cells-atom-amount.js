'use strict';
Polymer({

  is: 'cells-atom-amount',

  behaviors: [
    window.CellsBehaviors.AmountBehavior
  ],
  properties: {

    language: {
      type: String,
      value: function() {
        return document.documentElement.lang || 'en';
      }
    },
    /**
    * Number to split in integer, separator and fractional parts and represent
    */
    amount: Number,

    /**
    * ISO 4217 code for the currency
    */
    currencyCode: String,

    /**
    * ISO 4217 code for the local currency
    * A value must be provided in order to localice the currency symbols
    */
    localCurrency: {
      type: String,
      value: '' //empty string as absent value. This triggers computed properties although there is no valid value.
    },

    _currency: {
      type: String,
      computed: '_getCurrencyAsSymbol(localCurrency, currencyCode)'
    },

    /**
    * integer part of the amount
    */
    _integer: {
      type: String,
      computed: '_getAbsIntegerPart(amount, language)'
    },

    /**
    * fractional part of the amount
    */
    _fractional: {
      type: String,
      computed: '_getFractionalPart(amount)'
    },

    /**
    * separator between fractional and integer part
    */
    _separator: {
      type: String,
      computed: '_getSeparator(language)'
    },

    /**
    * Align currency symbol according to _currencyPosition
    */
    _alignRight: {
      type: Boolean,
      computed: '_isRightAligned(currencyCode)'
    },

    /**
    * Negative number or not
    */
    _isNegativeAmount: {
      type: Boolean,
      computed: '_isNegative(amount)',
      observer: '_toggleNegativeClass'
    },

    /**
     * Determine if '-' sign should be shown after currency symbol
     * @type {Object}
     */
    _minusAfterSymbol: {
      type: Boolean,
      computed: '_hasMinusAfterSymbol(localCurrency)'
    },

    /**
     * Show or hide '-' <span> before currency symbol
     * @type {Boolean}
     */
    _showMinusBeforeSymbol: {
      type: Boolean,
      value: false,
      computed: '_computeShowMinusBeforeSymbol(_isNegativeAmount, _minusAfterSymbol)'
    },

    /**
     * * Show or hide '-' <span> after currency symbol
     * @type {Boolean}
     */
    _showMinusAfterSymbol: {
      type: Boolean,
      value: false,
      computed: '_computeShowMinusAfterSymbol(_isNegativeAmount, _minusAfterSymbol)'
    },

    /**
     * * Used to hide/show component is there is a valid amount
     * @type {Boolean}
     */
    hidden: {
      type: Boolean,
      value: false,
      reflectToAttribute: true,
      computed: '_computeHidden(amount)'
    }

  },

  _computeHidden: function(amount) {
    return (amount === undefined || amount === null || isNaN(amount) || amount === '');
  },

  _computeShowMinusBeforeSymbol: function(_isNegative, _minusAfterSymbol) {
    return _isNegative && !_minusAfterSymbol;
  },

  _computeShowMinusAfterSymbol: function(_isNegative, _minusAfterSymbol) {
    return _isNegative && _minusAfterSymbol;
  },

  _toggleNegativeClass: function(_isNegative) {
    if (_isNegative) {
      this.classList.add('negative');
    } else {
      this.classList.remove('negative');
    }
  }

});
