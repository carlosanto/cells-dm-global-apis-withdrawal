'use strict';

var selectCurrencyCode = document.querySelector('#selectCurrencyCode');
var selectLocalCurrency = document.querySelector('#selectLocalCurrency');
var selectLanguage = document.querySelector('#selectLanguage');
var atomAmount = document.querySelector('cells-atom-amount');

selectCurrencyCode.addEventListener('change', function(e) {
  atomAmount.set('currencyCode', e.target.value);
});

selectLocalCurrency.addEventListener('change', function(e) {
  atomAmount.set('localCurrency', e.target.value);
});

selectLanguage.addEventListener('change', function(e) {
  atomAmount.set('language', e.target.value);
});
