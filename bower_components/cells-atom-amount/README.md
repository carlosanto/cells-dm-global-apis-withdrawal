# cells-atom-amount

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

`<cells-atom-amount>` is a component formatting an amount and its currency code into different combinations of sizes.

If amount is a negative value, .negative class is added to give a visual cue.

Example:

```html
<cells-atom-amount
  amount="6423525.45"
  currency-code="EUR"
  localCurrency="USD"
  language="en">
</cells-atom-amount>
```

## Styling

The following custom properties and mixins are available for styling:
