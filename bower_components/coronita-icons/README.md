# Coronita Iconset

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html) 

## Generating iconset

**Note: This process only applies to glomo-icons**. coronita-icons are using different viewbox (24x24).

### 1. Import latest selection
Go to [icomoon.io](https://icomoon.io/app/) > "Import Icons".
Select `selection.json` file.

### 2. Order icons by name
Select all the icons in the imported iconset and choose **"Rearrange icons"** from the upper right corner menu. Order by name.

### 3. Export iconset
Select all the icons in the iconset and choose **"Generate SVG & More"** from the bottom menu.

Click on **"Preferences"** and check **Polymer** as format. Other options are irrelevant when choosing Polymer as format and the icon size always is 1024.

![Icomoon Settings](docs/icomoon-settings.png)

## Updating iconset

Copy `<defs>` tag of `<icommon-downloaded-pack>/icomoon-polymer/icomoon-iconset-svg.html` and replace the same tag in `glomo-icons.html`. Reindent the document (2 spaces) and check the demo to make sure the iconset is Ok.

Replace `selection.json` file in this repo with the file downloaded in the Icomoon pack.

## Tips for getting better results

- Don't use transform attributes.
- Don't use `fill-rule="evenodd"`.
- Don't use fills to make holes.

Check out [Icomoon docs](https://icomoon.io/#docs/importing) for more information.

## Editing icons in Icommon
Choose the edit tool and select the icon to edit.

![Edit tool](docs/icomoon-edit.png)

**Center icons in canvas**

![Centering icon](docs/icomoon-center-icon.png)

**Fit to canvas**

![Fit to canvas](docs/icomoon-fit-canvas.png)
