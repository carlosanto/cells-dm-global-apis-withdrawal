var maps = {
  properties: [
    'endpoint',
    'sourceList',
    'destinationList',
    'fees',
    'lastResponse',
    'selectedSource',
    'selectedDestination',
    'accounts',
    'amount',
    'sourceBalance',
    'sourceCurrency',
    'date',
    'concept',
    'currentStep',
    'maxSteps',
    'references',
    //METHODS
    'doTransfer',
    'reset',
    'newOrder',
    'cleanUpState'
  ],
  stepper: {
    'selectedSource': {
      value: {
        id: '1234'
      },
      observer: '_selectedSrcObserver'
    },
    'selectedDestination': {
      value: {
        id: '1235'
      },
      observer: '_selectedDestObserver'
    },
    'amount': {
      value: 1000,
      observer: '_amountChanged'
    },
    'date': {
      value: (new Date()).toISOString(),
      observer: '_dateChanged'
    },
    'concept': {
      value: 'My Concept',
      observer: '_conceptChanged'
    }
  }
};
