var mocks = {
  selectedSource: 1,
  selectedDestination: 1,
  selectedAmount: {
    amount: 1000,
    currency: ''
  },
  accounts: [
    {
      accountId: '1234',
      number: 'ES90 0182 1642 0302 0153 0412',
      numberType: {
        id: 'IBAN',
        name: 'International Bank Account Number'
      },
      reminderCode: 'abc999',
      accountType: {
        id: 'CREDIT_CURRENCY_ACCOUNT',
        name: 'Credit currency account.'
      },
      title: {
        id: 'YOUNG_ACCOUNT',
        name: 'Young account.'
      },
      alias: 'Work account',
      openingDate: '2010-02-02',
      currencies: [
        {
          currency: 'EUR',
          isMajor: false
        },
        {
          currency: 'USD',
          isMajor: true
        }
      ],
      grantedCredits: [
        {
          amount: 3000,
          currency: 'EUR'
        },
        {
          amount: 3000,
          currency: 'USD'
        }
      ],
      availableBalance: {
        currentBalances: [
          {
            amount: 500,
            currency: 'EUR'
          },
          {
            amount: 556.05,
            currency: 'USD'
          }
        ],
        postedBalances: [
          {
            amount: 550,
            currency: 'EUR'
          },
          {
            amount: 611.66,
            currency: 'USD'
          }
        ],
        pendingBalances: [
          {
            amount: -50,
            currency: 'EUR'
          },
          {
            amount: -55.61,
            currency: 'USD'
          }
        ]
      },
      disposedBalance: {
        currentBalances: [
          {
            amount: 2500,
            currency: 'EUR'
          },
          {
            amount: 2443.95,
            currency: 'USD'
          }
        ],
        postedBalances: [
          {
            amount: 2450,
            currency: 'EUR'
          },
          {
            amount: 2388.34,
            currency: 'USD'
          }
        ],
        pendingBalances: [
          {
            amount: 50,
            currency: 'EUR'
          },
          {
            amount: 55.61,
            currency: 'USD'
          }
        ]
      },
      status: {
        id: 'BLOCKED',
        name: 'Account blocked'
      },
      image: {
        id: 'IMAGE_ACCOUNT',
        name: 'Image of the account',
        url: 'http://.../image.jpg'
      }
    },
    {
      accountId: '1235',
      number: 'ES90 0182 1642 0302 0153 7555',
      numberType: {
        id: 'IBAN',
        name: 'International Bank Account Number'
      },
      reminderCode: 'abc888',
      accountType: {
        id: 'COMMON_ACCOUNT',
        name: 'Common account.'
      },
      title: {
        id: 'YOUNG_ACCOUNT',
        name: 'Young account.'
      },
      alias: 'Joint account with my father',
      openingDate: '2011-09-01',
      currencies: [
        {
          currency: 'EUR',
          isMajor: false
        },
        {
          currency: 'USD',
          isMajor: true
        }
      ],
      availableBalance: {
        currentBalances: [
          {
            amount: 500,
            currency: 'EUR'
          },
          {
            amount: 556.05,
            currency: 'USD'
          }
        ],
        postedBalances: [
          {
            amount: 550,
            currency: 'EUR'
          },
          {
            amount: 611.66,
            currency: 'USD'
          }
        ],
        pendingBalances: [
          {
            amount: -50,
            currency: 'EUR'
          },
          {
            amount: -55.61,
            currency: 'USD'
          }
        ]
      },
      status: {
        id: 'ACTIVATED',
        name: 'Activated account'
      },
      image: {
        id: 'IMAGE_ACCOUNT',
        name: 'Image of the account',
        url: 'http://.../image.jpg'
      }
    }
  ],
  ownTransfer: {
    post: {
      request: {
        concept: 'Ipsum dolor sit amet, consetetur sadipscing',
        sender: {
          contract: {
            contractId: '2fac7cc3-b29e-4ae3-8380-66cf9a203f4c',
            number: 'bec6dc0a-475f-47b3-b385-e60d7f4d3936',
            numberType: {
              id: 'SPEI',
              name: '+5564758697'
            }
          }
        },
        receiver: {
          contract: {
            number: 'e3574au8-57ef-b347-bbf5-5b9fa1e98e90',
            numberType: {
              id: 'SPEI',
              name: '+5564758697'
            }
          }
        },
        sentMoney: [
          {
            amount: 123.32,
            currency: 'USD'
          }
        ]
      },
      response: {
        data: {
          ownTransferId: '5b9fa1e9-1c87-4d71-83b9-8e90de3574ac',
          concept: 'Pago de la tanda',
          sender: {
            contract: {
              contractId: 'bec6dc0a-475f-47b3-b385-e60d7f4d3936',
              number: '+629797292776',
              numberType: {
                id: 'SPEI',
                name: 'Spei'
              }
            }
          },
          receiver: {
            contract: {
              number: 'e3574au8-57ef-b347-bbf5-5b9fa1e98e90',
              numberType: {
                id: 'SPEI',
                name: '+5564758697'
              }
            }
          },
          sentMoney: [
            {
              amount: 132101534,
              currency: 'CHF',
              isMajor: true
            },
            {
              amount: 952225141,
              currency: 'UYU',
              isMajor: false
            }
          ],
          chargeAmount: {
            amount: 42329921,
            currency: 'TWD'
          },
          date: '2016-09-30T01:35:19-05:00',
          modifiedDate: '2016-09-30T01:35:19-05:00',
          accountingDate: '2016-09-30T01:35:19-05:00',
          fees: {
            totalFees: {
              amount: 2,
              currency: 'USD'
            },
            itemizeFees: [
              {
                feeType: 'DIRECT_MONTHLY',
                name: 'Direct monthly',
                amount: {
                  amount: 630004515,
                  currency: 'COP'
                },
                percentage: 337739178,
                whoPayFee: {
                  id: 'SENDER',
                  name: 'Sender'
                }
              },
              {
                feeType: 'DIRECT_MONTHLY',
                name: 'Direct monthly',
                amount: {
                  amount: 746334361,
                  currency: 'EUR'
                },
                percentage: 199417270,
                whoPayFee: {
                  id: 'BOTH',
                  name: 'Both'
                }
              },
              {
                feeType: 'DIRECT_MONTHLY',
                name: 'Direct monthly',
                amount: {
                  amount: 928258101,
                  currency: 'USD'
                },
                percentage: 251651457,
                whoPayFee: {
                  id: 'INTERMEDIARY_BANK',
                  name: 'Intermediary bank'
                }
              }
            ]
          },
          taxes: {
            totalTaxes: {
              amount: 891303266,
              currency: 'RUB'
            },
            itemizeTaxes: [
              {
                taxType: 'TARIFF',
                name: 'Tariff',
                amount: {
                  amount: 965292042,
                  currency: 'AUD'
                },
                percentage: 315626796
              },
              {
                taxType: 'PROPERTY',
                name: 'Property',
                amount: {
                  amount: 223908469,
                  currency: 'VEF'
                },
                percentage: 437783400
              },
              {
                taxType: 'PAYROLL',
                name: 'Payroll',
                amount: {
                  amount: 546196135,
                  currency: 'PEN'
                },
                percentage: 535021030
              }
            ]
          },
          exchangeRate: {
            baseCurrency: 'MXN',
            targetCurrency: 'EUR',
            date: '2016-09-29T19:30:08-05:00',
            values: {
              factor: {
                value: 0.0494410508,
                ratio: '49.44%'
              },
              type: 'SALE'
            }
          },
          status: {
            id: 'CANCELED',
            name: 'Canceled'
          },
          references: [
            {
              id: 'ref-1',
              name: 'Reference 1',
              value: 'Ref. Value 1'
            }
          ]
        }
      }
    }
  }
};
