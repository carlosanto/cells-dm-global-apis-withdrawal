/* global Polymer */
/* global CellsBehaviors */
(function(Polymer, CellsBehaviors) {

  'use strict';

  Polymer({

    is: 'cells-dm-global-apis-withdrawal',

    behaviors: [
      CellsBehaviors.StepManagerBehavior
    ],

    properties: {

      // CONFIG properties
      //-----------------------------------//

      /**
       * Chosen endpoint to make requests to.
       *
       * @type {String}
       */
      endpoint: {
        type: String,
        value: ''
      },

      // OUTPUT properties
      //-----------------------------------//

      /**
       * List of source products.
       *
       * @type  {Array}
       */
      sourceList: {
        notify: true,
        type: Array
      },

      /**
       * List of destination products.
       *
       * @type  {Array}
       */
      destinationList: {
        notify: true,
        type: Array
      },

      /**
       * Balance obtained from selected origin product.
       *
       * @type  {Object}
       */
      sourceBalance: {
        notify: true,
        type: Number
      },

      /**
       * Major source currency ISO code.
       *
       * @type  {String}
       */
      sourceCurrency: {
        notify: true,
        type: String
      },

      sourceAlias: {
        notify: true,
        type: String
      },
      sourceId: {
        notify: true,
        type: String
      },
      destinationAlias: {
        notify: true,
        type: String
      },
      destinationId: {
        notify: true,
        type: String
      },

      /**
       * Current operation fees.
       *
       * @type  {Object}
       */
      fees: {
        notify: true,
        type: Object
      },

      /**
       * Last response obtained from server.
       *
       * @type  {Object}
       */
      lastResponse: {
        notify: true,
        type: Object
      },

      /**
       * Transfer references obtained from server.
       *
       * @type  {Object}
       */
      references: {
        notify: true,
        type: Object
      },

      // INPUT properties
      //-----------------------------------//

      /**
       * Available accounts.
       *
       * @type  {Array}
       */
      accounts: {
        observer: '_accountsChanged',
        type: Array
      },

      /**
       * Available contacts.
       *
       * @type  {Array}
       */
      contacts: {
        type: Array,
        notify: true
      },

      customer: {
        type: Array,
        notify: true
      },

      /**
       * user logged
       */
      user: {
      type: Array,
      notify: true
      },

      /**
       * customer DP response
       */
      _usersResponse: {
      type: Array,
      observer: '_usersResponseObserver'
      },

      /**
       * customer DP error response
       */
      _usersError: {
      type: Array,
      observer: '_usersErrorObserver'
       },

       _userId: {
      type: String
      },

      environment: {
      type: Object
    },
      
      /**
       * ID of selected source.
       *
       * @type  {String}
       */
      selectedSource: {
        type: String
      },

      /**
       * ID of selected destination.
       *
       * @type  {String}
       */
      selectedDestination: {
        observer: '_selectedDestObserver',
        type: String
      },

      /**
       * Amount to transfer.
       * Must contain:
       *
       * @type      {Number}
       */
      amount: {
        observer: '_amountChanged',
        type: Number
      },

      /**
       * Concept of the transfer (optional)
       *
       * @type      {Object}
       */
      concept: {
        type: Object
      },

      // PRIVATE properties
      //-----------------------------------//

      /**
       * Concept value.
       *
       * @type  {String}
       * @private
       */
      _concept: {
        type: String
      },

      /**
       * Flags if service should simulate operation.
       * @todo --> move to behavior?
       *
       * @type  {Boolean}
       * @private
       */
      _isSimulating: {
        type: Boolean,
        value: true
      },

      /**
       * Flags if data provider is own transfer.
       *
       * @type  {Boolean}
       * @private
       */
      isOwn: {
        type: Boolean,
        value: false
      },

      /**
       * Flags if data provider is own transfer.
       *
       * @type  {Boolean}
       * @private
       */
      isInternal: {
        type: Boolean,
        value: false
      },

      /**
       * Flags if data provider is own transfer.
       *
       * @type  {Boolean}
       * @private
       */
      isInterbank: {
        type: Boolean,
        value: false
      },

      /**
       * Used for some countries.
       *
       * @type  {String}
       * @private
       */
      _reference: {
        type: String
      },

      /**
       * Stores the selected CM of the destination account.
       *
       * @type  {Object}
       * @private
       */
      _selectedDestination: {
        type: Object
      },

      /**
       * Stores the selected CM of the source account.
       *
       * @type  {Object}
       * @private
       */
      _selectedSource: {
        observer: '_internalSelectedSourceChanged',
        type: Object
      },

      /**
       * Request body to propagate into DP.
       *
       * @type  {Object}
       * @private
       */
      _requestBody: {
        type: Object
      },

      email: {
        type: String,
        value: ''
      },

      newContact: {
        type: Object
      }
    },

    listeners: {
      'register-step': 'registerStep',
      'summary-closed': 'doWithdrawal',
      'new-operation': 'reset'
    },

    observers: [
      '_getFees(_selectedSource, _selectedDestination, amount)',
      '_conceptChanged(concept.*)',
      '_selectedSrcObserver(sourceList, selectedSource)',
      'newContactObserver(evt)'
    ],

    /**
     * Stores DP reference.
     *
     * @property  _dataProvider
     * @type      {HTMLElement}
     * @private
     */
    _dataProvider: null,

    /**
     * Stores DP customer reference.
     *
     * @property  _dataProvider
     * @type      {HTMLElement}
     * @private
     */
    _customerDataProvider: null,

    ready: function() {
      this._dataProvider = this.$.dpWithdrawal;
      this._customerDataProvider = this.$.dpAsoUsers;
    },

    /**
     * Initializes the manager to its default status for new operation.
     *
     * @param force {Boolean}
     */
    newOrder: function(force) {
      if (this.lastResponse || force === true) {
        this.reset();
        if (this.selectedSource) {
          this.resetFollowingSteps(1);
        }
        this._selectedSrcObserver(this.sourceList, this.selectedSource);
      }
    },

    newContactObserver: function(contacto) {
      // this.newContact = true;
      console.log('---------jkdsajksadkjsad----------', contacto.data);
      this.newContact = contacto.data;
      // this._selectedDestination = contacto.data;
      this._selectedDestination = {
        name: contacto.data.name,
        number: contacto.data.products[0].number,
        numberType: contacto.data.products[0].numberType
      };
      console.log('newContactObserver ', this._selectedDestination);
      return this._setInternalProduct('', this.newContact, 'sourceList', '_selectedSource');
    },

    /**
     * Set default values.
     */
    reset: function() {

      //PUBLIC
      this.set('sourceList', this.accounts);
      this.set('destinationList', null);
      this.set('selectedDestination', null);
      this.set('amount', '');
      this.set('email', '');
      this.set('concept', null);
      this.set('references', null);
      this.set('fees', null);
      this.set('lastResponse', null);
      this.set('sourceAlias', '');
      this.set('sourceId', '');
      this.set('destinationAlias', '');
      this.set('destinationId', '');
      this.set('sourceBalance', null);
      this.set('sourceCurrency', '');
      this.set('isOwn', false);
      this.set('isInternal', false);
      this.set('isInterbank', false);

      //BEHAVIOR
      this.set('lastActiveStep', null);
      this.set('currentStep', 1);
      this.set('canExecute', false);

      //PRIVATE
      this.set('_isSimulating', true);
      this.set('_selectedSource', null);
      this.set('_selectedDestination', null);
      this.set('_concept', '');
      this.set('_reference', '');
      this.set('_requestBody', null);
    },

    /**
     * Cleans DM current status
     */
    cleanUpState: function() {
      this.newOrder(true);
    },

    /**
     * Performs the operation.
     */
    doWithdrawal: function(e) {
      this.set('canExecute', false);
      this.set('_isSimulating', false);
      this._buildRequestBody();
      
      this._dataProvider.generateRequest().then(function(response) {
        console.log("response", response);
        this.set('lastResponse', ((response && response.data) || response));
        this.set('references', this.lastResponse && this.lastResponse.references);
      }.bind(this));
    },

    getUser: function() {
      this._userId = "MXDEV";
      this._customerDataProvider.generateRequest();
    },

    _usersResponseObserver: function(response) {
      this.set('user', response.data);
    },

  _usersErrorObserver: function(error) {
    //To define
  },

    /**
     * Returns the CM object from the <em>orig</em> identified by <em>id</em> and stores the result
     * into <em>prop</em>.
     *
     * @param   id {String} Product ID.
     * @param   orig {Array} Of products to search from.
     * @param   prop {String} Where to store match.
     * @returns {Boolean} Whether there's a match or not.
     * @private
     */
    _setInternalProduct: function(id, contactos, customer, orig, prop) {
      var isData;
      var match;
      this.email = '';
      if (id !== "") {
        if (typeof id === 'object') {
          id = id.accountId || id.id;
        }
        if (typeof id === 'string') {
          var sources = this.get(orig);
          if (!Array.isArray(sources)) {
            sources = (sources && sources.items) || [];
          }
          sources.some(function(account) {
            match = account.accountId === id && account;


            if(match === false){
              for (var i = 0; i < contactos.length; i++) {
                var contactInfo = contactos[i].contactsInformation;
                var flagContact = false;
                for (var j = 0; j < contactInfo.length; j++){
                  var productId = contactInfo[j].contactInformationId;
                  var contactTipo = contactInfo[j].contactType;
                  var emailExist = contactTipo.id.search('EMAIL');
                  if(emailExist >= 0){
                    isData = contactTipo.name;
                  }
                  var phoneNumberExist = productId.search(id);
                  if(phoneNumberExist >= 0){
                    match = contactInfo[j];
                    match.number = contactInfo[j].value;
                    match.alias = contactos[i].alias;
                  }
                }
              }
            }
            if(match === false){
              if(id === "100001"){
                match = customer;
                match.alias = customer.firstName;
                var contactDetails = customer.contactDetails;
                for (var k = 0; k < contactDetails.length; k++) {
                  var contactDetailsId = contactDetails[k].contactType.id;
                  var phoneNumberExist = contactDetailsId.search('PHONE_NUMBER');
                  if(phoneNumberExist >= 0){
                    match.number = contactDetails[k].contact;
                  }
                }
              }
            }
            return !!match;
          });
        }
      }else{
        console.log('contactos', this.newContact);
        match = this.newContact;
        return !!match;
      }
      this.email = isData;
      this.set(prop, match);
      console.log("match", match);
      return !!match;
    },

    /**
     * Sets the alias and id outputs for confirmation screen of both source and destination.
     *
     * @param   from {String} 'source' or 'destination'
     * @private
     */
    _setAliasAndID: function(from) {
      if (from && typeof from === 'string' && (from === 'source' || from === 'destination')) {
        var source = this['_selected' + (from.charAt(0).toUpperCase() + from.substr(1))];
        if (source) {
          this.set(from + 'Alias', source.alias || source.name);
          this.set(from + 'Id', source.number || source.id);
        }
      }
    },

    /**
     * Observer for property <em>accounts</em>.
     */
    _accountsChanged: function() {
      if (this.accounts) {
        this.set('sourceList', this.accounts);
      }
    },

    /**
     * Observer for property <em>selectedSrc</em>.
     *
     * @param   source {Number} Order position of the selected product source.
     * @param   sources {Array}
     * @private
     */
    _selectedSrcObserver: function(sources, source) {
      if (sources && this._setInternalSelectedSource(source)) {
        this._nextStep();
      }
    },

    /**
     * Obtains the CM of the selected account and stores it into `_selectedSource`.
     * Returns <em>true</em> if account is found.
     *
     * @param   id {*} Could be the account model or the ID.
     * @returns {Boolean}
     * @private
     */
    _setInternalSelectedSource: function(id) {
      var contactos = this.contacts;
      var customer = this.user;
      return this._setInternalProduct(id, contactos, customer, 'sourceList', '_selectedSource');
    },

    /**
     * Callback executed on `_selectedSource` changes.
     *
     * @private
     */
    _internalSelectedSourceChanged: function() {
      this._setDestinationList();
      this._setSourceBalance();
      this._setAliasAndID('source');
    },

    /**
     * Eliminates current selected source from sources and set resultant array as available destination.
     *
     * @private
     */
    _setDestinationList: function() {
      var source = this.get('selectedSource');
      var sourceList = this.get('sourceList');
      var destinationList = {
        items: []
      };
      if (sourceList) {
        (sourceList.items || sourceList).forEach(function(product, index) {
          if (product.accountId !== source) {
            destinationList.items.push(product);
          }
        });
      }
      this.set('destinationList', destinationList);
    },

    /**
     * Retrieves current balance of selected source from sources list.
     *
     * @private
     */
    _setSourceBalance: function() {
      var balance = '';
      var currency = '';
      var source = this.get('_selectedSource');

      //Find major currency out from source
      if (source !== null && source !== undefined && typeof source === 'object') {
        var balances = source.availableBalance && source.availableBalance.currentBalances;
        var currencies = source.currencies;
        if (Array.isArray(balances) && Array.isArray(currencies)) {
          currencies.some(function(item) {
            currency = item.isMajor ? item.currency : undefined;
            return !!currency;
          });
          if (currency) {
            balances.some(function(item) {
              balance = (item.currency === currency)
                ? item.amount
                : undefined;
              return balance !== undefined;
            });
          }
        }
      }

      //Set results
      this.set('sourceBalance', balance);
      this.set('sourceCurrency', currency);
    },

    /**
     * Observer for property <em>_selectedDest</em>.
     *
     * @param   id {String} Product ID
     * @private
     */
    // _selectedDestObserver: function(id, orig, prop) {
      _selectedDestObserver: function(id) {
      //@todo: validate is not same as origin (shouldn't be)a
      var contactos = this.contacts;
      var customer = this.user;
      this._setInternalProduct(id, contactos, customer, 'destinationList', '_selectedDestination');
      this._setAliasAndID('destination');
      this._nextStep();
    },

    /**
     * Observer for <em>amount.amount</em> value.
     *
     * @param   amount {Number} Transfer amount.
     * @private
     */
    _amountChanged: function(amount) {
      this._nextStep();
    },

    /**
     * Observer for <em>concept</em>.
     *
     * @param   payload {String} Transfer concept
     * @private
     */
    _conceptChanged: function(payload) {
      var concept = payload.value || payload;
      this._concept = (concept && concept.concept) || '';
      this._reference = (concept && concept.reference) || null;
      this._nextStep();
    },

    /**
     * Observer fired whenever selected source, destination, amount, date or current operation fees
     * changes.
     *
     * Evaluates if minimum required info is populated, and the tries to obtain current fees for this operation.
     * @param source {Object} Source product
     * @param destination {Object} Destination Product
     * @param amount {Number} Amount to be transfered
     * @param date {String} Date of the operation
     *
     * @private
     */
    _getFees: function(source, destination, amount) {
      if (!!source && !!destination && !!amount && (typeof date === 'string')) {
        this.set('_isSimulating', true);
        this._buildRequestBody();
        this._dataProvider.generateRequest().then(function(response) {
          if (response && response.response !== undefined) {
            response = response.response;
          }
          response = (response && response.data) || response;
          this.set('fees', response.fees || null);
          console.log('fees ', response.fees);
        }.bind(this));
      }
    },

    /**
     * Builds value of <em>_requestBody</em> with current operation values.
     *
     * @private
     */
    _buildRequestBody: function() {
      console.log('destination ', this._selectedDestination);
      console.log('source ', this._selectedSource);
      this.set('_requestBody', {
        concept: this._concept,
        sender: {
          contract: {
            contractId: this._selectedSource.accountId,
            number: this._selectedSource.number,
            numberType: {
              id: "CCC"
            },
            product: {
              id: "ACCOUNTS"
            }
          }
        },
        receiver: {
           mobile: "+6705613273575"
        },
        sentMoney: [
          {
            amount: this.amount,
            currency: this.sourceCurrency
          }
        ],
        dueDate: "2016-09-29T19:30:08-05:00",
        retriesMaximumNumber: 10,
        availabityTime: 180,
        notification: [
          {
            notificationType: "EMAIL",
            value: "micorreo@jotmail.com"
          },
          {
            notificationType: "EMAIL",
            value: "tucorreo@yuju.com"
          },
          {
            notificationType: "SMS",
            value: "+3595558424235"
          }
        ],
        whoPayFee: {
          id: "DESTINATION_BANK"
        }
     });
    }

    /**
     * Fired after reaching last active step
     * @event 'cells-scroll-zone'
     */
  });
}(Polymer, CellsBehaviors));